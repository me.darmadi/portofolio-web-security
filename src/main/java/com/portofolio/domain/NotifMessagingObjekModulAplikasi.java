package com.portofolio.domain;

import com.portofolio.notification.Ignore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotifMessagingObjekModulAplikasi {
	
	public static String table() {
		return "NotifMessagingObjekModulAplikasi_S";
	}
	
	Integer kdProfile;
	Integer kdNotifMessaging;
	String titleNotifikasi;
	String pesanNotifikasi;
	String customURLObjekModul;
	String namaFungsiFrontEnd;
	
	String kdRuanganTujuan;
	String KdJabatanTujuan;
	String kdModulAplikasiTujuan;
	String kdObjekModulAplikasiTujuan;
	Integer hanyaUntukAtasan;

	@Ignore
	ObjekModulAplikasi objekModulAplikasi;

	@Ignore
	Ruangan ruangan;
	
//	public ModulAplikasi modulAplikasi;
//

}
