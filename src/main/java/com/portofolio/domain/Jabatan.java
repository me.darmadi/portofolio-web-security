package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class Jabatan {
	
	public static String table() {
		return "Jabatan_M";
	}

	public String getKode() {
    	return kdJabatan;
    }
    
	public void setKode(String kode) {
    	this.kdJabatan = kode;
    }
	
	private Integer kdProfile;
	private String kdJabatan;
	private String namaJabatan;
	
	
}
