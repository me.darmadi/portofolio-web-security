package com.portofolio.domain;

import com.portofolio.domain.base.BaseProfile;

import lombok.Getter;
import lombok.Setter;

/**
*
* 
*/
@Getter
@Setter
public class MapObjekModulToModulAplikasi extends BaseProfile {
	
	public static String table() {
		return "MapObjekModulToModulAplikasi_S";
	}
	
	public void setKode(String kode) {
		this.kdObjekModulAplikasi = kode;
	}
	
	public String getKode() {
		return kdObjekModulAplikasi;
	}

	private String kdObjekModulAplikasi;
	private String kdModulAplikasi;
	private Integer isEncrypted;
	private Integer noUrutObjek;
	
}
