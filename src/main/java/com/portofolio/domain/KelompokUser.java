package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class KelompokUser  {
	
	public Integer getKode() {
    	return kdKelompokUser;
    }
    
	public void setKode(Integer kode) {
    	this.kdKelompokUser = kode;
    }
		

    private Integer kdKelompokUser;

    private String kelompokUser;
    
    public static String table() {
		return "KelompokUser_S";
	}
    
	
}
