package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MapLoginUserToPegawai {

	public static String table() {
		return "MapLoginUserToPegawai_S";
	}
	
	private long kdUser;
	private Integer kdProfile;
	private Integer kdKelompokUser;
	private String kdPegawai;

	//private Boolean statusEnabled;
}
