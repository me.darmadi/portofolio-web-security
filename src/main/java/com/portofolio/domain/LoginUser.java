package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginUser {
	
	public static String table() {
		return "LoginUser_S";
	}
	
	Long kdUser;
	String namaUserMobile;
	String namaUserEmail;
	String kataSandi;
	String hintKataSandi;
	String jenisKelamin;
//	Long tglDaftar;
	Long tglLahir;
	Long tglDaftar;
	String namaLengkap;
//	String tglLahir;
//	String noIdentitas;
//	String alamatLengkap;
	String pathFileImageUser;
//	Integer kdJenisKelamin;
//	Integer kdNegara;
//	Integer kdPropinsi;
//	Integer kdKotaKabupaten;
//	Integer kdDesaKelurahan;
//	Integer kdKecamatan;
//	String kodePos;
//	Integer kdZonaWaktu;	
	boolean statusEnabled;
	
//	Boolean isLocked;
//	String strEmailAct;
//	String strMobAct;
//	Long lastAskReset;
//	Integer lastTry;

}
