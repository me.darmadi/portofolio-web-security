package com.portofolio.domain;

import com.portofolio.domain.base.BaseTransaksiDepartemen;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Pasien extends BaseTransaksiDepartemen {
	
	public static String table() {
		return "Pasien_M";
	}
	
	public String getKode() {
    	return noCM;
    }
    
	public void setKode(String kode) {
    	this.noCM = kode;
    }
	
	private Integer kdProfile;
	private String noCM;
	private Integer isTemporary;
	private Integer kdAgama;
	private String kdDepartemen;
	private Integer kdGolonganDarah;
	private Integer kdHubunganKeluarga;
	private String kdJabatan;
	private Integer kdJenisKelamin;
	private Integer kdKelompokUmur;
	private Integer kdLokasiDokumenRM;
	private Integer kdNegara;
	private String kdPegawai;
	private Integer kdPekerjaan;
	private String kdPendidikan;
	private Integer kdStatusPerkawinan;
	private Integer kdSuku;
	private Integer kdTitle;
	private String nIPPenjabatTandaTgnKK;
	private String nPWP;
	private String namaKeluarga;
	private String namaKepalaKK;
	private String namaLengkap;
	private String namaPanggilan;
	private String noCMHead;
	private String noIdentitas;
	private String noKK;
	private String noMasukMeninggal;
	private String pejabatTandaTgnKK;
	private String photoDiri;
	private Integer qtyImunisasi;
	private String statusRhesus;
	private String tempatLahir;
	private Long tglDaftar;
	private Long tglKK;
	private Long tglLahir;
	private Long tglTemporary;
}
