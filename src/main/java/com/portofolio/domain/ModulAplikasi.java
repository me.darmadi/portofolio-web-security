package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;

/**
*
* 
*/
@Getter
@Setter
public class ModulAplikasi {

	public static String table() {
		return "ModulAplikasi_S";
	}
	
	public void setKode(String kode) {
		this.kdModulAplikasi = kode;
	}
	
	public String getKode() {
		return kdModulAplikasi;
	}
	
	private String kdModulAplikasi;
	private String modulAplikasi;
	private String ModulIconImage;
	private String ModulNoUrut;
	private Boolean statusEnabled;
	
}
