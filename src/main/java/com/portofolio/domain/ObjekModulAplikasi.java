package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class ObjekModulAplikasi  {
	
	public static String table() {
		return "ObjekModulAplikasi_S";
	}
	
	public void setKode(String kode) {
		this.kdObjekModulAplikasi = kode;
	}
	
	public String getKode() {
		return kdObjekModulAplikasi;
	}
	
	private String kdObjekModulAplikasi;
	private String alamatURLFormObjek;
	private String kdObjekModulAplikasiHead;
	private String objekModulAplikasi;
	
//	private Boolean showInMobile;
//	private Boolean active;

}
