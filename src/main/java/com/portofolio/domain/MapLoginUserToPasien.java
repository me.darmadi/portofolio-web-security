package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MapLoginUserToPasien {
	
	public static String table() {
		return "MapLoginUserToPasien_S";
	}
	
	private long kdUser;
	private Integer kdProfile;
	private Integer kdKelompokUser;
	private String noCM;

	private Boolean statusEnabled;

}
