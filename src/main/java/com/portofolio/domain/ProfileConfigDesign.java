package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class ProfileConfigDesign {
	
	public static String table() {
		return "ProfileConfigDesign_S";
	}

	
	private String kdModulAplikasi;
	private Integer kdProfile;
	private Integer kdVersion;
	private Integer kdBahasa;
	
	
}
