package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotifMessaging {

	public static String table() {
		return "NotifMessaging_S";
	}

	private Integer kdProfile;
	private Integer kdNotifMessaging;
	private String kdModulAplikasi;
	private String kdObjekModulAplikasi;
	
}
