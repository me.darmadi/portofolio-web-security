package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class Profile {
	
	public static String table() {
		return "Profile_M";
	}
	
	public Integer getKode() {
    	return kdProfile;
    }
    
	public void setKode(Integer kode) {
    	this.kdProfile = kode;
    }
	
	private Integer kdProfile;
	private Integer kdNegara;
	private String namaLengkap;
		
}
