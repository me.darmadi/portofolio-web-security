package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class Ruangan  {
	
	public static String table() {
		return "Ruangan_M";
	}
	
	public String getKode() {
    	return kdRuangan;
    }
    
	public void setKode(String kode) {
    	this.kdRuangan = kode;
    }

	private Integer kdProfile;
	private String kdRuangan;
	private String namaRuangan;
	private String kdDepartemen;

}
