package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class MapObjekModulToKelompokUser {

	public void setKode(String kode) {
		this.kdObjekModulAplikasi = kode;
	}
	
	public String getKode() {
		return kdObjekModulAplikasi;
	}
	
	private Integer kdProfile;
	private String kdModulAplikasi;
	private String kdObjekModulAplikasi;
	private Integer kdKelompokUser;
	private Integer tampil;
	private Integer cetak;
	private Integer simpan;
	private Integer ubah;
	private Integer hapus;

//	private String kodeExternal;
//	private String namaExternal;	
}
