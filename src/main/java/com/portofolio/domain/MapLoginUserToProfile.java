package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MapLoginUserToProfile {

	public static String table() {
		return "MapLoginUserToProfile_S";
	}
	
	private long kdUser;
	private Integer kdProfile;
	private String kdRuangan;
	private String kdModulAplikasi;

	//private Boolean statusEnabled;

}
