package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class Departemen {
	
	public static String table() {
		return "Departemen_M";
	}

	public String getKode() {
    	return kdDepartemen;
    }
    
	public void setKode(String kode) {
    	this.kdDepartemen = kode;
    }

	private Integer kdProfile;
	private String kdDepartemen;
	private String namaDepartemen;
	
}
