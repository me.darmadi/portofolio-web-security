package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;

/**
*
* 
*/
@Getter
@Setter
public class Pegawai {

	public static String table() {
		return "Pegawai_M";
	}

	public String getKode() {
		return kdPegawai;
	}

	public void setKode(String kode) {
		this.kdPegawai = kode;
	}

	private Integer kdProfile;
	private String kdPegawai;
	private String kdJabatan;
	private String fingerPrintID;
	private String namaLengkap;
	private String namaPanggilan;
	private String photoDiri;
	private Integer kdNegara;
	private Integer kdNegaraAsal;
	private Integer kdStatusPegawai;
	private String kdDepartemen;
}
