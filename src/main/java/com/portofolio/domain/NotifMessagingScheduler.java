package com.portofolio.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotifMessagingScheduler {
	
	public static String table() {
		return "NotifMessagingScheduler_S";
	}
	
	Integer kdProfile;
	Integer kdMessaging;
	String kdPegawai;
	
	Long tglKirim;
	boolean terkirim;
	String kdRuangan;
	String kdRuanganTujuan;
	Integer statusEnabled;
	

}
