package com.portofolio.domain.base;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseProfile {

	@NotNull(message="basemasterdto.kdprofile.notnull")
	private Integer kdProfile;

	@NotNull(message="basemasterdto.statusenabled.notnull")
	public Boolean statusEnabled;
	
	@NotNull(message="basemasterdto.norec.notnull")
	private String noRec;
	
}
