package com.portofolio.domain.base;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseMaster extends BaseProfile {
		
	@Size(min=4,max=100,message="basemasterdto.reportdisplay.size")
	@NotNull(message="basemasterdto.reportdisplay.notnull")
	private String reportDisplay;

	@Size(max=10, message = "basemasterdto.kodeexternal.size")
	@NotNull(message="basemasterdto.kodeexternal.notnull")
	private String kodeExternal;

	@Size(max=100, message = "basemasterdto.namaexternal.size")
	@NotNull(message="basemasterdto.namaexternal.notnull")
	private String namaExternal;
	
}
