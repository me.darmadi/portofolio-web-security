package com.portofolio.domain.base;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseMasterInteger extends BaseMaster {
	
	@NotNull(message="basemasterdto.primaryid.notnull")
	private Integer kode;

}
