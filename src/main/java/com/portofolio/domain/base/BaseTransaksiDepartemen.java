package com.portofolio.domain.base;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseTransaksiDepartemen extends BaseProfile {

	@NotNull(message="basemasterdto.kddepartemen.notnull")
	private String kdDepartemen;
}
