package com.portofolio.domain.base;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseMasterDepartemen extends BaseMaster {

	@NotNull(message="basemasterdto.kddepartemen.notnull")
	private String kdDepartemen;
}
