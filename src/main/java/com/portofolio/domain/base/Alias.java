package com.portofolio.domain.base;

public @interface Alias {
	String value();
}
