package com.portofolio.domain.base;

public @interface RealColumn {

	String value();
}
