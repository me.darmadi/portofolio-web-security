package com.portofolio.security;

import io.jsonwebtoken.MalformedJwtException;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.fasterxml.jackson.core.JsonParseException;
import com.portofolio.constant.SecurityConstant;

/**
 * StatelessAuthenticationFilter class
 */
public class StatelessAuthenticationFilter extends GenericFilterBean {
	private final TokenAuthenticationService authenticationService;

	public StatelessAuthenticationFilter(TokenAuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		
//		
//		String allow = "origin, content-type, accept, x-requested-with, remember-me," + SecurityConstant.AUTH_HEADER_NAME.toLowerCase();
//
////
//		res.setHeader("Access-Control-Allow-Credentials", "true");
//		res.setHeader("Access-Control-Allow-Origin", "*");
//		res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
//		res.setHeader("Access-Control-Max-Age", "3600");
//		res.setHeader("Access-Control-Request-Headers", allow);
//		res.setHeader("Access-Control-Expose-Headers", allow);
//		res.setHeader("Access-Control-Allow-Headers", allow);

		//String token = req.getHeader(SecurityConstant.AUTH_HEADER_NAME);
		
//		res.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
		res.setHeader("Access-Control-Max-Age", "3600");
//		res.setHeader("Access-Control-Allow-Origin", req.getRemoteHost());
		
		Authentication authentication = null;
		try {
			authentication = authenticationService.getAuthentication(req);
		} catch (JsonParseException | MalformedJwtException e) {
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			httpResponse.addHeader(SecurityConstant.MessageInfo.ERROR_MESSAGE, "Error Token (Not Valid Token)");
			filterChain.doFilter(request, response);
		}
		try {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			filterChain.doFilter(request, response);
			SecurityContextHolder.getContext().setAuthentication(null);
		} catch (Exception e) {
			// TODO: handle exception
			logger.info("not found for url : " + req.getRequestURI());
		}

	}
}
