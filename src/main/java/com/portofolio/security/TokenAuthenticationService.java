package com.portofolio.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;

import com.fasterxml.jackson.core.JsonParseException;
import com.portofolio.constant.SecurityConstant;

/**
 * TokenAuthenticationService class
 * 
 * @author Adik & Syamsu
 */
public class TokenAuthenticationService {

	public static TokenHandler tokenHandler;

	public TokenAuthenticationService() {
	}

	public TokenAuthenticationService(String secret, UserService userService) {
		tokenHandler = new TokenHandler(secret, userService);
	}

	@AppPermission({ AppPermission.SAVE, AppPermission.UPDATE })
	public String addAuthentication(HttpServletResponse response, UserAuthentication authentication) {
		final User user = authentication.getDetails();
		return tokenHandler.createTokenForUser(user);
	}

	@AppPermission({ AppPermission.SAVE, AppPermission.UPDATE })
	public Boolean cekAuth(HttpServletRequest request) throws JsonParseException {
		return getAuthentication(request) != null;
	}

	public Authentication getAuthentication(HttpServletRequest request) throws JsonParseException {
		String tokenParam;
		// Untuk report PDF sementara
		try {
			final String[] tokens = request.getQueryString().split("&");
			for (String tokenTemp : tokens) {
				if (tokenTemp.toLowerCase().indexOf(SecurityConstant.HEADER_X_AUTH_TOKEN.toLowerCase()) >= 0) {
					tokenParam = tokenTemp.split("=")[1];
					final User user = tokenHandler.parseUserFromToken(tokenParam);
					if (user != null) {
						return new UserAuthentication(user);
					}
				}
			}
		} catch (Exception e) {
		}

		String token1, token2;

		try {
			token1 = request.getHeader(SecurityConstant.HEADER_AUTHORIZATION.toLowerCase());
			token2 = request.getHeader(SecurityConstant.HEADER_X_AUTH_TOKEN.toLowerCase());
			
			boolean toke1 = token1 == null || "".equals(token1.trim()) || "aa".equals(token1.trim().toLowerCase());
			boolean toke2 = token2 == null || "".equals(token2.trim()) || "aa".equals(token2.trim().toLowerCase());
			
			if (toke1) {
				if (toke2) {
					return null;
				}
				token1 = token2;
			}

			final User user = tokenHandler.parseUserFromToken(token1);
			if (user != null) {
				return new UserAuthentication(user);
			}
		} catch (Exception e) {
//			StringUtil.printStackTrace(e);
		}

		return null;
	}
}
