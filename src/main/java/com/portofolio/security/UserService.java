package com.portofolio.security;

import java.util.Arrays;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.portofolio.dao.mapper.AuthMapper;
import com.portofolio.domain.Pegawai;
import com.portofolio.dto.AuthDto;
import com.portofolio.dto.SessionDto;
import com.portofolio.service.AuthService;
import com.portofolio.service.LoginUserService;
import com.portofolio.util.CommonUtil;


public class UserService implements org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	SqlSessionFactory sqlSessionFactory;

	@Autowired
	private LoginUserService userService;
	
	@Autowired
	AuthMapper authMapper;


	private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

	public UserService() {
		super();
	}

	@Override
	public final User loadUserByUsername(String namaUser) throws UsernameNotFoundException {

		AuthService authService = userService;

		SessionDto sessionDto = userService.toSessionDto(namaUser);

		authService = userService;
		AuthDto authDto = authService.buildAuthDtoFromSession(sessionDto);

		String sessString = authService.toSessionJson(authDto);
		String encrypted = authDto.getEncrypted();

		GrantedAuthority authority = new SimpleGrantedAuthority("USER");
		UserDetails userDetails = (UserDetails) new User(sessString, encrypted, Arrays.asList(authority));
		detailsChecker.check(userDetails);
		return (User) userDetails;
	}
}
