package com.portofolio.security;

import org.springframework.security.core.userdetails.User;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
//import java.security.Key;
//import io.jsonwebtoken.security.Keys;

// versi baru
//public class TokenHandler {
//	private static String myTok = "UFQuIEphc2FtZWRpa2EgU2FyYW5hdGFtYSBUb2tlbiBTZWN1cml0eSBmb3IgUFQuIFNlbnRyYWwgTGluayBTb2x1dGlvbnMgQXJ0aGEgR3JhaGE=";
//	private static Key key = Keys.hmacShaKeyFor(Base64.decode(myTok.getBytes()));
//	private final String secret;
//	private final UserService userService;
//
//	public TokenHandler(String secret, UserService userService) {
//		this.secret = secret;
//		this.userService = userService;
//	}
//
//	public User parseUserFromToken(String token) {
//		String username = Jwts.parser()
//				.setSigningKey(secret)
//				.parseClaimsJws(token)
//				.getBody()
//				.getSubject();
//		
//		return userService.loadUserByUsername(username);
//	}
//
//	public String createTokenForUser(User user) {
//		return Jwts.builder()
//				.setHeaderParam("typ", "JWT")
//				.setSubject(user.getUsername())
//				.signWith(key, SignatureAlgorithm.HS512)
//				.compact();
//	}
//}

//versi lama
public class TokenHandler {
	private final String secret;
	private final UserService userService;

	public TokenHandler(String secret, UserService userService) {
		this.secret = secret;
		this.userService = userService;
	}

	public User parseUserFromToken(String token) {
		String username = Jwts.parser()
				.setSigningKey(secret)
				.parseClaimsJws(token)
				.getBody()
				.getSubject();
		
		return userService.loadUserByUsername(username);
	}

   public String createTokenForUser(User user) {
       return Jwts.builder().setHeaderParam("typ", "JWT")
               .setSubject(user.getUsername())
               .signWith(SignatureAlgorithm.HS512, secret)
               .compact();
   }
}