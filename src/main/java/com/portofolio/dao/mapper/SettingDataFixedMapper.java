package com.portofolio.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface SettingDataFixedMapper {

	@Select("Select nilaiField From SettingDataFixed_M "
			+ " Where namaField=#{namaField} "
			+ " and kdProfile=#{kdProfile} "
			+ " and StatusEnabled=1 ")
	String getStringSettingDataFixed(@Param("kdProfile") Integer kdProfile, @Param("namaField") String namaField) throws RuntimeException;
}
