package com.portofolio.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import com.portofolio.dao.provider.NotificationMessagingMessagingMapperProvider;
import com.portofolio.domain.NotifMessaging;
import com.portofolio.domain.NotifMessagingObjekModulAplikasi;
import com.portofolio.domain.NotifMessagingScheduler;
import com.portofolio.domain.ObjekModulAplikasi;
import com.portofolio.domain.Ruangan;

@Mapper
public interface NotificationMessagingMapper {

	@SelectProvider(type = NotificationMessagingMessagingMapperProvider.class, 
			method = "queryFindRuanganByKode")
	Ruangan findRuanganByKode(@Param("kdProfile") Integer kdProfile, @Param("kdRuangan") String kdRuangan) throws RuntimeException;

	@SelectProvider(type = NotificationMessagingMessagingMapperProvider.class, 
			method = "queryFindObjekModulAplikasiByKode")
	ObjekModulAplikasi findObjekModulAplikasiByKode(@Param("kdObjekModulAplikasi") String kdObjekModulAplikasi) throws RuntimeException;

	@SelectProvider(type = NotificationMessagingMessagingMapperProvider.class, 
			method = "queryFindNotifMessagingByObjekModulAplikasi")
	List<NotifMessaging> findNotifMessagingByObjekModulAplikasi(@Param("kdProfile") Integer kdProfile,  
			@Param("kdModulAplikasi") String kdModulAplikasi,
			@Param("kdObjekModulAplikasi") String kdObjekModulAplikasi) throws RuntimeException;

//	@SelectProvider(type = NotificationMessagingMessagingMapperProvider.class, 
//			method = "queryFindNotifMessagingByUrlOrObjekModulAplikasi")
//	NotifMessaging findNotifMessagingByUrlOrObjekModulAplikasi(@Param("kdProfile") Integer kdProfile,  
//	@Param("kdkModulAplikasi") String kdModulAplikasi,
//	@Param("kdObjekModulAplikasi") String kdObjekModulAplikasi, @Param("uRI") String uRI) throws RuntimeException;
	
	@SelectProvider(type = NotificationMessagingMessagingMapperProvider.class, 
			method = "queryFindByMessagingAndRuangan")
	List<NotifMessagingObjekModulAplikasi> findByNotifMessagingAndRuangan(@Param("kdProfile") Integer kdProfile, 
			@Param("lKdNotifMessaging")  List<Integer> lKdNotifMessaging, 
			@Param("lKdRuanganTujuan") List<String> lKdRuanganTujuan) throws RuntimeException;
	
	@SelectProvider(type = NotificationMessagingMessagingMapperProvider.class, 
			method = "queryFindByMessagingAndJabatan")
	List<NotifMessagingObjekModulAplikasi> findByNotifMessagingAndJabatan(@Param("kdProfile") Integer kdProfile, 
			@Param("lKdNotifMessaging")  List<Integer> lKdNotifMessaging) throws RuntimeException;
	
	@SelectProvider(type = NotificationMessagingMessagingMapperProvider.class, 
			method = "queryFindByNotifMessaging")
	List<NotifMessagingObjekModulAplikasi> findByNotifMessaging(@Param("kdProfile") Integer kdProfile, 
			@Param("lKdNotifMessaging")  List<Integer> lKdNotifMessaging) throws RuntimeException;

	@SelectProvider(type = NotificationMessagingMessagingMapperProvider.class, 
			method = "queryInsertNotifMessagingScheduler")
	int addNotifMessagingScheduler(NotifMessagingScheduler vo) throws RuntimeException;
	
	@SelectProvider(type = NotificationMessagingMessagingMapperProvider.class, 
			method = "queryJabatanAtasan")
	String getJabatanAtasan(@Param("kdProfile") Integer kdProfile,  @Param("kdJabatanAsal") String kdJabatanAsal) throws RuntimeException;
}
