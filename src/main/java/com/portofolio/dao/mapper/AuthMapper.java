package com.portofolio.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import com.portofolio.dao.provider.AuthProvider;
import com.portofolio.domain.Departemen;
import com.portofolio.domain.Jabatan;
import com.portofolio.domain.KelompokUser;
import com.portofolio.domain.LoginUser;
import com.portofolio.domain.ModulAplikasi;
import com.portofolio.domain.Pasien;
import com.portofolio.domain.Pegawai;
import com.portofolio.domain.Profile;
import com.portofolio.domain.ProfileConfigDesign;
import com.portofolio.domain.Ruangan;
import com.portofolio.dto.AuthDto;
import com.portofolio.dto.SessionDto;


@Mapper
public interface AuthMapper {
	
	@SelectProvider(type = AuthProvider.class, method = "queryLoginUserByKdUserOrPhoneNumber")
	LoginUser queryLoginUserByKdUserOrPhoneNumber(@Param("namaUserMobile") String namaUserMobile, @Param("namaUserEmail") String namaUserEmail) throws RuntimeException;

	@SelectProvider(type = AuthProvider.class, method = "queryLoginUserByKdUser")
	LoginUser getLoginUser(@Param("kdUser") Long kdUser) throws RuntimeException;

	//////////////////////////////////////////
	
	@SelectProvider(type = AuthProvider.class, method = "queryAllProfile")
	List<Profile> getAllProfile(LoginUser loginUser) throws RuntimeException;

	@SelectProvider(type = AuthProvider.class, method = "queryAllModulAplikasi")
	List<ModulAplikasi> getAllModulAplikasi(AuthDto authDto);

	@SelectProvider(type = AuthProvider.class, method = "queryAllRuangan")
	List<Ruangan> getAllRuangan(@Param("kdProfile") Integer kdProfile, @Param("kdModulAplikasi") String kdModulAplikasi, @Param("kdUser") Long kdUser, @Param("kdLokasi") String kdLokasi) throws RuntimeException;
	
	@SelectProvider(type = AuthProvider.class, method = "queryAllLokasi")
	List<Ruangan> getAllLokasi(AuthDto authDto) throws RuntimeException;
	
	@SelectProvider(type = AuthProvider.class, method = "queryLokasi")
	Ruangan getLokasi(SessionDto authDto) throws RuntimeException;

	/////////////////////////////////////////
		
	@SelectProvider(type = AuthProvider.class, method = "queryPegawai")
	Pegawai getPegawai(SessionDto authDto) throws RuntimeException;

	@SelectProvider(type = AuthProvider.class, method = "querySuperVisor")
	Pegawai getSuperVisor(SessionDto authDto) throws RuntimeException;

	@SelectProvider(type = AuthProvider.class, method = "queryPasien")
	Pasien getPasien(SessionDto authDto) throws RuntimeException;

	@SelectProvider(type = AuthProvider.class, method = "queryProfile")
	Profile getProfile(SessionDto authDto) throws RuntimeException;
	
	@SelectProvider(type = AuthProvider.class, method = "queryModulAplikasi")
	ModulAplikasi getModulAplikasi(SessionDto authDto) throws RuntimeException;

	/////////////////////////////////////////
	
	@SelectProvider(type = AuthProvider.class, method = "queryDepartemen")
	Departemen getDepartemen(SessionDto authDto) throws RuntimeException;
	
	@SelectProvider(type = AuthProvider.class, method = "queryKelompokUser")
	KelompokUser getKelompokUser(SessionDto authDto) throws RuntimeException;

	@SelectProvider(type = AuthProvider.class, method = "querySuperKelompokUser")
	KelompokUser getSuperKelompokUser(SessionDto authDto) throws RuntimeException;
	
	@SelectProvider(type = AuthProvider.class, method = "querySettingDataFixed")
	String getSettingDataFixed(@Param("kdProfile") Integer kdProfile, @Param("namaField") String namaField);
	
	@SelectProvider(type = AuthProvider.class, method = "queryMapLoginUserToProfileByPegawai")
	Pegawai queryMapLoginUserToProfileByPegawai(@Param("kdUser") Long kdUser);

	/////////////////////////////////////////
	
	@SelectProvider(type = AuthProvider.class, method = "queryJabatan")
	Jabatan getJabatan(Pegawai pegawai) throws RuntimeException;

	@Select("select KdVersion from ProfileConfigDesign_S where KdProfile = #{kdProfile} and KdModulAplikasi = #{kdModulAplikasi} and StatusEnabled=1 ")
	Integer getVersionFromProfileConfig(@Param("kdProfile") Integer kdProfile, @Param("kdModulAplikasi") String kdModulAplikasi);

	@Select("select KdBahasa from MapLoginUserToProfile_S where KdUser = #{kdUser} and KdProfile = #{kdProfile} and KdModulAplikasi = #{kdModulAplikasi} and KdRuangan = #{kdRuangan} and StatusEnabled=1 ")	
	Integer getBahasaFromMapLoginUserToProfile(@Param("kdUser")Long kdUser,@Param("kdProfile") Integer kdProfile, @Param("kdModulAplikasi") String kdModulAplikasi,@Param("kdRuangan") String kdRuangan);

	@SelectProvider(type = AuthProvider.class, method = "queryRuangan")
	Ruangan getRuangan(SessionDto sessionDto);

	@SelectProvider(type = AuthProvider.class, method = "queryProfileConfigDesign")
	ProfileConfigDesign getProfileConfigDesign(SessionDto sessionDto);




}
