package com.portofolio.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.cache.annotation.Cacheable;

import com.portofolio.dao.provider.AppPermissionProvider;
import com.portofolio.domain.MapObjekModulToKelompokUser;
import com.portofolio.domain.ObjekModulAplikasi;
@Mapper
public interface AppPermissionMapper {

	@SelectProvider(type = AppPermissionProvider.class, method = "queryPermission")
	@Cacheable("AppPermissionMappergetPermission")
	List<MapObjekModulToKelompokUser> getPermission(@Param("kdObjekModulAplikasi") String kdObjekModulAplikasi, 
			@Param("kdKelompokUser") Integer kdKelompokUser, 
			@Param("kdModulAplikasi") String kdModulAplikasi,
			@Param("kdProfile") Integer kdProfile) throws RuntimeException;

	@SelectProvider(type = AppPermissionProvider.class, method = "queryFindByAlamatURLFormObjek")
	@Cacheable("AppPermissionMapperfindByAlamatUrlForm")
	ObjekModulAplikasi findByAlamatUrlForm(@Param("alamatURLFormObjek") String alamatURLFormObjek) throws RuntimeException;
}
