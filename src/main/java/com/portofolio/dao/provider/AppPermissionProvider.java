package com.portofolio.dao.provider;

import java.util.Map;

import com.portofolio.domain.MapObjekModulToKelompokUser;
import com.portofolio.domain.ObjekModulAplikasi;
import com.portofolio.helper.DBUtil;
public class AppPermissionProvider {

	public String queryPermission(Map<String, Object> parameters) {
		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT ");
		sb.append(DBUtil.extract(MapObjekModulToKelompokUser.class, "A"));
		sb.append(" FROM MapObjekModulToKelompokUser_S A ");
		sb.append(" WHERE A.kdObjekModulAplikasi = #{kdObjekModulAplikasi} ");
		sb.append(" AND A.KdKelompokUser = #{kdKelompokUser} ");
		sb.append(" AND A.kdModulAplikasi = #{kdModulAplikasi} ");
		sb.append(" AND A.KdProfile = #{kdProfile} ");
		sb.append(" AND A.statusEnabled = 1 ");

		return sb.toString();
	}
	
//	public String queryPermission2(Map<String, Object> parameters) {
//		StringBuilder sb = new StringBuilder();
//
//		sb.append(" SELECT ");
//		sb.append(DBUtil.extract(MapObjekModulToKelompokUser.class, "A"));
//		sb.append(" FROM MapObjekModulToKelompokUser_S A ");
//		sb.append(" WHERE A.kdObjekModulAplikasi = #{kdObjekModulAplikasi} ");
//		sb.append(" AND A.KdKelompokUser = #{kdKelompokUser} ");
//		sb.append(" AND A.KdKelompokUser = #{kdKelompokUser} ");
//		sb.append(" AND A.KdProfile = #{kdProfile} ");
//		sb.append(" AND A.statusEnabled = 1 ");
//
//		return sb.toString();
//	}

	public String queryFindByAlamatURLFormObjek(Map<String, Object> parameters) {
		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT ");
		sb.append(DBUtil.extract(ObjekModulAplikasi.class, "A"));
		sb.append(" FROM ObjekModulAplikasi_S A ");
		sb.append(" WHERE A.AlamatURLFormObjek = #{alamatURLFormObjek} ");
		sb.append(" AND A.statusEnabled = 1 ");

		return sb.toString();
	}
}
