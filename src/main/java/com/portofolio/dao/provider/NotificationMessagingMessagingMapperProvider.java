package com.portofolio.dao.provider;

import java.util.List;
import java.util.Map;

import com.google.common.base.Joiner;
import com.portofolio.domain.Jabatan;
import com.portofolio.domain.NotifMessaging;
import com.portofolio.domain.NotifMessagingObjekModulAplikasi;
import com.portofolio.domain.ObjekModulAplikasi;
import com.portofolio.domain.Ruangan;
import com.portofolio.helper.DBUtil;

public class NotificationMessagingMessagingMapperProvider {
	
	
	public String queryJabatanAtasan(Map<String, Object> parameters){
		StringBuilder sb = new StringBuilder();
		
		sb.append(" SELECT KdJabatanHead FROM ");
		sb.append(Jabatan.table());
		sb.append(" WHERE KdJabatan = #{kdJabatanAsal}");
		sb.append(" AND KdProfile = #{kdProfile}");
		sb.append(" AND statusEnabled = 1 ");
				
		return sb.toString();
	}
	
	@SuppressWarnings("unchecked")
	public String queryFindByNotifMessaging(Map<String, Object> parameters) {
		
		List<String> lKdNotifMessaging = (List<String>) parameters.get("lKdNotifMessaging");	
		String inKdNotifMessaging = Joiner.on(",").join(lKdNotifMessaging);
				
		StringBuilder sb = new StringBuilder();
		
		sb.append(" SELECT ");
		sb.append(DBUtil.extract(NotifMessagingObjekModulAplikasi.class, "A"));
		sb.append(" FROM "); 
		sb.append(NotifMessagingObjekModulAplikasi.table());
		sb.append (" A ");
		sb.append(" WHERE A.KdNotifMessaging IN (");
		sb.append(inKdNotifMessaging);
		sb.append(") "); 
		sb.append(" AND A.KdProfile = #{kdProfile}");
		sb.append(" AND A.statusEnabled = 1 ");
				
		return sb.toString();
	}
	
	@SuppressWarnings("unchecked")
	public String queryFindByMessagingAndJabatan(Map<String, Object> parameters) {

		List<String> lKdNotifMessaging = (List<String>) parameters.get("lKdNotifMessaging");
		
		String inKdNotifMessaging = Joiner.on(",").join(lKdNotifMessaging);

		StringBuilder sb = new StringBuilder();
		
		sb.append(" SELECT ");
		sb.append(DBUtil.extract(NotifMessagingObjekModulAplikasi.class, "A"));
		sb.append(" FROM "); 
		sb.append(NotifMessagingObjekModulAplikasi.table());
		sb.append (" A ");
		sb.append(" WHERE ");
		sb.append(" A.KdJabatanTujuan IS NOT NULL "); 
		sb.append(" AND A.KdNotifMessaging IN (");
		sb.append(inKdNotifMessaging);
		sb.append(") "); 
		sb.append(" AND A.KdProfile = #{kdProfile}");
		sb.append(" AND A.statusEnabled = 1 ");
				
		return sb.toString();	}
	
	@SuppressWarnings("unchecked")
	public String queryFindByMessagingAndRuangan(Map<String, Object> parameters) {
		
		List<String> lKdNotifMessaging = (List<String>) parameters.get("lKdNotifMessaging");
		List<String> lKdRuanganTujuan = (List<String>) parameters.get("lKdRuanganTujuan");
		
		String inKdNotifMessaging = Joiner.on(",").join(lKdNotifMessaging);
		String inKdRuangan = Joiner.on(",").join(lKdRuanganTujuan);

		StringBuilder sb = new StringBuilder();
		
		sb.append(" SELECT ");
		sb.append(DBUtil.extract(NotifMessagingObjekModulAplikasi.class, "A"));
		sb.append(" FROM "); 
		sb.append(NotifMessagingObjekModulAplikasi.table());
		sb.append (" A ");
		sb.append(" WHERE ");
		sb.append(" A.KdJabatanTujuan IN (");
		sb.append(inKdRuangan);
		sb.append(") "); 
		sb.append(" AND A.KdNotifMessaging IN (");
		sb.append(inKdNotifMessaging);
		sb.append(") "); 
		sb.append(" AND A.KdProfile = #{kdProfile}");
		sb.append(" AND A.statusEnabled = 1 ");
				
		return sb.toString();
	}
	
	
	public String queryFindRuanganByKode(Map<String, Object> parameters) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(" SELECT ");
		sb.append(DBUtil.extract(Ruangan.class, "A"));
		sb.append(" FROM " + Ruangan.table() + " A ");
		sb.append(" WHERE A.KdRuangan = #{kdRuangan} ");
		sb.append(" AND A.KdProfile = #{kdProfile}");
		sb.append(" AND A.statusEnabled = 1 ");
		
		return sb.toString();
	}

	public String queryFindObjekModulAplikasiByKode(Map<String, Object> parameters) {
		
		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT ");
		sb.append(DBUtil.extract(ObjekModulAplikasi.class, "A"));
		sb.append(" FROM " + ObjekModulAplikasi.table() + " A ");
		sb.append(" WHERE A.KdObjekModulAplikasi = #{kdObjekModulAplikasi} ");
		sb.append(" AND A.statusEnabled = 1 ");

		return sb.toString();
	}
	
	
	public String queryFindNotifMessagingByObjekModulAplikasi(Map<String, Object> parameters) {
		
		StringBuilder sb1 = new StringBuilder();
		
		sb1.append(" SELECT ");
		sb1.append(DBUtil.extract(NotifMessaging.class, "A"));
		sb1.append(" FROM " + NotifMessaging.table() + " A ");
		sb1.append(" WHERE A.KdObjekModulAplikasi = #{kdObjekModulAplikasi} ");
		sb1.append(" AND A.KdModulAplikasi = #{kdModulAplikasi}");
		sb1.append(" AND A.KdProfile = #{kdProfile}");
		sb1.append(" AND A.statusEnabled = 1 ");
		
		StringBuilder sb2 = new StringBuilder();
		
		sb2.append(" SELECT KdNotifMessaging ");
		sb2.append(" FROM " + NotifMessaging.table() + " A ");
		sb2.append(" WHERE A.KdObjekModulAplikasi = #{kdObjekModulAplikasi} ");
		sb2.append(" AND A.KdModulAplikasi = #{kdModulAplikasi}");
		sb2.append(" AND A.KdProfile = #{kdProfile}");
		sb2.append(" AND A.statusEnabled = 1 ");

		StringBuilder sb = new StringBuilder(sb1);
		
		sb.append(" UNION ");

		sb.append(" SELECT ");
		sb.append(DBUtil.extract(NotifMessaging.class, "A"));
		sb.append(" FROM " + NotifMessaging.table() + " A ");
		sb.append(" WHERE A.KdNotifMessagingHead IN ( ");
		sb.append(sb2);
		sb.append(") ");
		sb.append(" AND A.statusEnabled = 1 ");
		
		//SELECT * FROM [dbo].[NotifMessaging_S] A  WHERE A.KdObjekModulAplikasi = '000503' UNION SELECT * FROM [dbo].[NotifMessaging_S] A  WHERE A.KdObjekModulAplikasi IN  (SELECT B.KdObjekModulAplikasi FROM [dbo].[NotifMessaging_S] B WHERE A.KdObjekModulAplikasi = '000503') 
		
		return sb.toString();
	}
	
	public String queryFindNotifMessagingByUrlOrObjekModulAplikasi(Map<String, Object> parameters) {
		
		StringBuilder sb1 = new StringBuilder();
		
		sb1.append(" SELECT ");
		sb1.append(DBUtil.extract(NotifMessaging.class, "A"));
		sb1.append(" FROM " + NotifMessaging.table() + " A ");
		sb1.append(" LEFT JOIN " + ObjekModulAplikasi.table() + " B ");
		sb1.append(" ON A.KdObjekModulAplikasi = B.KdObjekModulAplikasi " );
		sb1.append(" WHERE (A.KdObjekModulAplikasi = #{kdObjekModulAplikasi} ");
		sb1.append(" OR B.alamatURLFormObjek = #{uRI}) ");
		sb1.append(" AND A.KdModulAplikasi = #{kdModulAplikasi}");
		sb1.append(" AND A.KdProfile = #{kdProfile}");
		sb1.append(" AND A.statusEnabled = 1 ");
		
		StringBuilder sb2 = new StringBuilder();
		
		sb2.append(" SELECT KdNotifMessaging ");
		sb2.append(DBUtil.extract(NotifMessaging.class, "A"));
		sb2.append(" FROM " + NotifMessaging.table() + " A ");
		sb2.append(" LEFT JOIN " + ObjekModulAplikasi.table() + " B ");
		sb2.append(" ON A.KdObjekModulAplikasi = B.KdObjekModulAplikasi " );
		sb2.append(" WHERE (A.KdObjekModulAplikasi = #{kdObjekModulAplikasi} ");
		sb2.append(" OR B.alamatURLFormObjek = #{uRI}) ");
		sb2.append(" AND A.KdModulAplikasi = #{kdModulAplikasi}");
		sb2.append(" AND A.KdProfile = #{kdProfile}");
		sb2.append(" AND A.statusEnabled = 1 ");
		
		StringBuilder sb = new StringBuilder(sb1);
		
		sb.append(" UNION ");
		
		sb.append(" SELECT ");
		sb.append(DBUtil.extract(NotifMessaging.class, "A"));
		sb.append(" FROM " + NotifMessaging.table() + " A ");
		sb.append(" WHERE A.KdNotifMessagingHead IN ( ");
		sb.append(sb2);
		sb.append(") ");
		sb.append(" AND A.KdModulAplikasi = #{kdModulAplikasi}");
		sb.append(" AND A.statusEnabled = 1 ");
		
		return sb.toString();
	}
		
	public String queryInsertNotifMessagingScheduler(Map<String, Object> parameters) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(" INSERT INTO ");
		sb.append(NotifMessagingObjekModulAplikasi.table());
		sb.append(" ( ");
		sb.append(DBUtil.extract(NotifMessagingObjekModulAplikasi.class, ""));
		sb.append(" ) VALUES (");
		sb.append(DBUtil.extractParam(NotifMessagingObjekModulAplikasi.class));
		sb.append(" )");
		
		return sb.toString();
	}
}
