package com.portofolio.dao.provider;

import java.util.Map;

import com.portofolio.domain.Departemen;
import com.portofolio.domain.Jabatan;
import com.portofolio.domain.KelompokUser;
import com.portofolio.domain.LoginUser;
import com.portofolio.domain.MapLoginUserToPasien;
import com.portofolio.domain.MapLoginUserToPegawai;
import com.portofolio.domain.MapLoginUserToProfile;
import com.portofolio.domain.ModulAplikasi;
import com.portofolio.domain.Pasien;
import com.portofolio.domain.Pegawai;
import com.portofolio.domain.Profile;
import com.portofolio.domain.ProfileConfigDesign;
import com.portofolio.domain.Ruangan;
import com.portofolio.helper.DBUtil;

public class AuthProvider {
	
	public String queryLoginUserByKdUserOrPhoneNumber(Map<String, Object> parameters){

		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(LoginUser.class, "A")
		+ " FROM "+ LoginUser.table() + " A "
		+ " WHERE A.NamaUserMobile=#{namaUserMobile} "
		+ " OR A.NamaUserEmail=#{namaUserEmail}"
		+ " AND A.StatusEnabled = 1";
		
		
		return sb;
	}
	
	public String queryLoginUserByKdUser(Map<String, Object> parameters){

		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(LoginUser.class, "A")
		+ " FROM "+ LoginUser.table() + " A "
		+ " WHERE A.KdUser=#{kdUser} "
		+ " AND A.StatusEnabled = 1";
		
		
		return sb;
	}
	
	
	
	public String queryAllProfile(){
		
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(Profile.class,"A")	
		+ " FROM " + Profile.table() + " A "
		+ " LEFT JOIN " + MapLoginUserToProfile.table() + " B "
		+ " ON A.kdProfile = B.kdProfile"
		+ " WHERE B.KdUser=#{kdUser}"
		+ " AND A.StatusEnabled = 1"
		+ " AND B.StatusEnabled = 1";
		
		
		return sb;
	}
	
	public String queryAllModulAplikasi() {
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(ModulAplikasi.class,"A")	
		+ " FROM " + ModulAplikasi.table() + " A "
		+ " LEFT JOIN " + MapLoginUserToProfile.table() + " B "
		+ " ON A.kdModulAplikasi = B.kdModulAplikasi "
		+ " WHERE B.KdUser=#{kdUser} AND B.kdProfile=#{kdProfile}"
		+ " AND A.statusEnabled = 1"
		+ " AND B.statusEnabled = 1";
		
		
		return sb;
	}
	
	public String queryModulAplikasi() {
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(ModulAplikasi.class,"A")	
		+ " FROM " + ModulAplikasi.table() + " A "
		+ " where A.statusEnabled = 1 and A.kdModulAplikasi=#{kdModulAplikasi}";
		
		
		return sb;
	}
	
	public String queryMapLoginUserToProfileByPegawai() {
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(Pegawai.class,"p")	
		+  MapLoginUserToProfile.table()+" m " + 
		"left join "+LoginUser.table()+" l on l.KdUser=m.KdUser " + 
		"left join "+Pegawai.table()+" p on p.KdPegawai=m.KdPegawai " + 
		"where l.KdUser=#{kdUser} and m.StatusEnabled=1";
		
		
		return sb;
	}
	
	public String queryAllLokasi() {
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(Ruangan.class,"D")	
		+ " FROM "  + Ruangan.table() + " D "
		+ " LEFT JOIN " + MapLoginUserToProfile.table() + " B "				
		+ " ON D.KdRuangan = B.KdRuangan "
		+ " AND D.KdProfile = B.KdProfile"
		+ " WHERE B.KdUser=#{kdUser} AND B.kdProfile=#{kdProfile} "
		+ " AND D.KdRuanganHead is null "
		+ " AND B.StatusEnabled = 1"
		+ " AND D.StatusEnabled = 1";
		
		return sb;
	}
	
	public String queryLokasi() {
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(Ruangan.class,"A")	
		+ " FROM " + Ruangan.table() + " A "
		+ " WHERE A.kdRuangan=#{kdLokasi} AND A.kdProfile=#{kdProfile}"
		+ " AND A.StatusEnabled = 1";
		
		return sb;
	}
	
	
	
	public String queryAllRuangan() {
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(Ruangan.class,"A")	
		+ " FROM " + Ruangan.table() + " A "
		+ " LEFT JOIN " + MapLoginUserToProfile.table() + " B "
		+ " ON A.KdRuangan = B.KdRuangan "
		+ " AND A.KdProfile = B.KdProfile"
//		+ " LEFT JOIN " + ModulAplikasi.table() + " C "
//		+ " ON B.KdModulAplikasi = C.KdModulAplikasi "
		+ " WHERE B.KdUser=#{kdUser} AND B.kdProfile=#{kdProfile} "
		+ " AND A.KdRuanganHead=#{kdLokasi}"
//		+ " AND B.KdModulAplikasi=#{kdModulAplikasi}"
		+ " AND A.StatusEnabled = 1"
		+ " AND B.StatusEnabled = 1";
//		+ " AND C.StatusEnabled = 1";
		
		return sb;
	}
	
	//////////////////////////////////////
	
	public String queryPegawai(){
		
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(Pegawai.class, "A")
		+ " FROM " + Pegawai.table() + " A "
		+ " LEFT JOIN " + MapLoginUserToPegawai.table() + " B " 
		+ " ON A.kdProfile = B.kdProfile AND A.kdPegawai = B.kdPegawai "	
		+ " WHERE B.KdUser=#{kdUser} AND A.KdProfile=#{kdProfile}"
		+ " AND A.TglMasuk is not null "
		+ " AND A.StatusEnabled = 1 "
		+ " AND B.StatusEnabled = 1 ";
		
		return sb;
	}
	
	public String querySuperVisor(){
		
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(Pegawai.class, "A")
		+ " FROM " + Pegawai.table() + " A "
		+ " LEFT JOIN " + MapLoginUserToPegawai.table() + " B " 
		+ " ON A.kdProfile = B.kdProfile AND A.kdPegawai = B.kdPegawai "	
		+ " WHERE B.KdUser=#{suKdUser} AND A.KdProfile=#{kdProfile}"
		+ " AND A.StatusEnabled = 1"
		+ " AND B.StatusEnabled = 1";
		
		return sb;
	}
	
	public String queryPasien(){
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(Pasien.class, "A")
		+ " FROM " + Pasien.table() + " A "
		+ " LEFT JOIN " + MapLoginUserToPasien.table() + " B " 
		+ " ON A.kdProfile = B.kdProfile AND A.NoCM = B.NoCM "	
		+ " WHERE B.KdUser=#{kdUser} AND A.KdProfile=#{kdProfile}"
		+ " AND A.StatusEnabled = 1"
		+ " AND B.StatusEnabled = 1";
		
		return sb;
	}
	
	public String queryProfile(/*Map<String, Object> parameters*/) {
		String sb = " SELECT DISTINCT "				
		+ DBUtil.extract(Profile.class,"A")
		+ " FROM " + Profile.table() + " A "
		+ " WHERE A.KdProfile=#{kdProfile}"
		+ " AND A.statusEnabled = 1";
				
		return sb;
	}

	public String querySettingDataFixed(Map<String, Object> parameters) {
		String sb = "SELECT NilaiField FROM SettingDataFixed_M "
				+ " WHERE Kdprofile = #{kdProfile} "
				+ " AND NamaField = #{namaField}";
		
		return sb;
	}
	
	public String queryDepartemen(/*Map<String, Object> parameters*/) {
//		String sb = " SELECT DISTINCT "
//		+ DBUtil.extract(Departemen.class,"A")
//		+ " FROM " + Departemen.table() + " A "
//		+ " WHERE A.KdDepartemen=#{kdDepartemen} AND A.KdProfile=#{profile}"
//		+ " AND A.statusEnabled = 1";
		
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(Departemen.class,"A")
		+ " FROM " + Departemen.table() + " A "
		+ " LEFT JOIN " + Ruangan.table() + " B "
		+ " ON A.KdDepartemen = B.KdDepartemen "
		+ " AND A.KdProfile = B.KdProfile "
		+ " LEFT JOIN " + MapLoginUserToProfile.table() + " C "
		+ " ON B.KdRuangan = C.KdRuangan "
		+ " AND B.KdProfile = C.KdProfile "
		+ " WHERE C.kdUser = #{kdUser} "
		+ " AND C.KdProfile = #{kdProfile} "
		+ " AND C.KdRuangan = #{kdRuangan} "
		+ " AND C.kdModulAplikasi = #{kdModulAplikasi} "
		+ " AND A.statusEnabled = 1"
		+ " AND B.statusEnabled = 1"
		+ " AND C.StatusEnabled = 1";
		
		return sb;
	}
	
	public String queryRuangan() {
		String sb = " SELECT DISTINCT "			
		+ DBUtil.extract(Ruangan.class, "A")
		+ " FROM " + Ruangan.table() + " A "
		+ " WHERE A.KdRuangan=#{kdRuangan} AND A.KdProfile=#{kdProfile}"
		+ " AND A.statusEnabled = 1";
		
		return sb;
	}
	
	public String queryKelompokUser() {

//		String sb = " SELECT DISTINCT "
//		+ DBUtil.extract(KelompokUser.class,"A")
//		+ " FROM " + KelompokUser.table() + " A "
//		+ " WHERE A.KdKelompokUser=#{kdKelompokUser} AND A.KdProfile=#{kdProfile}"
//		+ " AND A.statusEnabled = 1";
		
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(KelompokUser.class,"A")
		+ " FROM " + KelompokUser.table() + " A "
		+ " LEFT JOIN " + MapLoginUserToPegawai.table() + " B "
		+ " ON A.KdKelompokUser = B.KdKelompokUser "
		+ " AND A.KdProfile = B.KdProfile "
		+ " WHERE B.KdPegawai = #{kdPegawai} "
		+ " AND B.KdUser = #{kdUser}"
		+ " AND B.KdProfile = #{kdProfile}"
		+ " AND A.statusEnabled = 1"
		+ " AND B.statusEnabled = 1";

				
		return sb;
	}
	
	public String querySuperKelompokUser() {

//		String sb = " SELECT DISTINCT "
//		+ DBUtil.extract(KelompokUser.class,"A")
//		+ " FROM " + KelompokUser.table() + " A "
//		+ " WHERE A.KdKelompokUser=#{kdKelompokUser} AND A.KdProfile=#{kdProfile}"
//		+ " AND A.statusEnabled = 1";
		
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(KelompokUser.class,"A")
		+ " FROM " + KelompokUser.table() + " A "
		+ " LEFT JOIN " + MapLoginUserToPegawai.table() + " B "
		+ " ON A.KdKelompokUser = B.KdKelompokUser "
		+ " AND A.KdProfile = B.KdProfile "
		+ " WHERE B.KdPegawai = #{kdPegawai} "
		+ " AND B.KdUser = #{suKdUser}"
		+ " AND B.KdProfile = #{kdProfile}"
		+ " AND A.statusEnabled = 1"
		+ " AND B.statusEnabled = 1";

				
		return sb;
	}

	public String queryJabatan() {
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(Jabatan.class,"A")
		+ " FROM " + Jabatan.table() + " A"
		+ " WHERE A.KdJabatan=#{kdJabatan} AND A.KdProfile=#{kdProfile}"
		+ " AND A.statusEnabled = 1";
		
		return sb;
	}
	
	public String queryProfileConfigDesign() {
		String sb = " SELECT DISTINCT "
		+ DBUtil.extract(ProfileConfigDesign.class,"A")
		+ " FROM " + ProfileConfigDesign.table() + " A"
		+ " WHERE A.KdModulAplikasi=#{kdModulAplikasi} AND A.KdProfile=#{kdProfile}"
		+ " AND A.statusEnabled = 1";
		
		return sb;
	}
	
	
}
