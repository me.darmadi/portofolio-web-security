package com.portofolio.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.portofolio.domain.Departemen;
import com.portofolio.domain.Jabatan;
import com.portofolio.domain.KelompokUser;
import com.portofolio.domain.ModulAplikasi;
import com.portofolio.domain.Pasien;
import com.portofolio.domain.Pegawai;
import com.portofolio.domain.Profile;
import com.portofolio.domain.Ruangan;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class AuthDto {
	String namaUser;
	String kdDepartemen;
	Integer kdNegaraProfile;
	Integer kdNegaraPegawai;
	Integer kdKelompokUser;
	String encrypted;
	Long kdUser;	
	Integer kdProfile; 
	String kdModulAplikasi;
	String kdLokasi;
	String kdRuangan;
	String kdPegawai;
	Integer kdBahasa;
	Integer kdVersion;
	String kdJabatan;
	Ruangan lokasi;
	Ruangan ruangan;
	Profile profile;
	ModulAplikasi modulAplikasi;
	KelompokUser kelompokUser;
	List<Profile> profiles;	
	List<ModulAplikasi> modulAplikasis;	
	List<Ruangan> lokasis;
	List<Ruangan> ruangans;
	Map<String,Object> result=new HashMap<>();
	Jabatan jabatan;
	Departemen departemen;
	Pegawai pegawai;
	
}
