package com.portofolio.dto;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class PermissionDto {

	boolean tampil;
	boolean cetak;
	boolean simpan;
	boolean ubah;
	boolean hapus;

}
