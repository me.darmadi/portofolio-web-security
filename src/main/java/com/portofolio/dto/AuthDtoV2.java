package com.portofolio.dto;

import java.util.List;

import com.portofolio.domain.Departemen;
import com.portofolio.domain.Jabatan;
import com.portofolio.domain.KelompokUser;
import com.portofolio.domain.ModulAplikasi;
import com.portofolio.domain.Pasien;
import com.portofolio.domain.Pegawai;
import com.portofolio.domain.Profile;
import com.portofolio.domain.Ruangan;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class AuthDtoV2 {
	
	String info;

	String namaUser;
	String encrypted;
	
	Long kdUser;	
	Long suKdUser;
	
	String kdHistoryLoginModulAplikasi;
	
	Profile profile;	
	ModulAplikasi modulAplikasi;	
	Ruangan lokasi;
	Ruangan ruangan;
	Departemen departemen;
	
	///////////// PEGAWAI
	Integer kdProfile; 
	Integer kdNegaraProfile;
	Integer kdNegaraPegawai;
	
	
	String kdModulAplikasi;
	String kdLokasi;
	String kdRuangan;

	String kdPegawai; 
	String kdDepartemen; 
	String kdJabatan;
	Integer kdKelompokUser; 
	
	String superKdPegawai;
	Integer superKdKelompokUser;

	Integer kdBahasa;
	Integer kdVersion;
	
	Pegawai pegawai;
	Jabatan jabatan;
	KelompokUser kelompokUser;

	Pegawai superPegawai;
	KelompokUser superKelompokUser;

	///////////// PASIEN
	
	String noCM;
	Pasien pasien;
	
	List<Profile> profiles;	
	List<ModulAplikasi> modulAplikasis;	
	List<Ruangan> lokasis;
	List<Ruangan> ruangans;
	
}
