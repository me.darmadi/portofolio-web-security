package com.portofolio.dto;

import lombok.Getter;
import lombok.Setter;

/**
*
* 
*/
@Getter
@Setter
public class ModulAplikasiDto {

	String kdModulAplikasi;
	String modulAplikasi;
	String ModulIconImage;
	String ModulNoUrut;
	
}
