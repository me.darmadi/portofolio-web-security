package com.portofolio.dto;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class SessionDto {
	
	String info;
	
	String namaUser;
	String encrypted;
	
	Long kdUser;	
	
	String kdHistoryLoginModulAplikasi;
	
	Integer kdStatusPegawai;
	
	Integer kdProfile;
	Integer kdNegaraProfile;
	Integer kdNegaraPegawai;
	String kdModulAplikasi;
	String kdLokasi;
	String kdRuangan;
	
	String kdPegawai;
	String kdDepartemen;
	String kdJabatan;
	Integer kdKelompokUser;

	Integer kdBahasa;
	Integer kdVersion;
	
}
