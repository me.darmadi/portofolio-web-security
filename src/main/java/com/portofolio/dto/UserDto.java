/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portofolio.dto;

import java.util.List;

import com.portofolio.domain.Departemen;
import com.portofolio.domain.Jabatan;
import com.portofolio.domain.KelompokUser;
import com.portofolio.domain.ModulAplikasi;
import com.portofolio.domain.Pegawai;
import com.portofolio.domain.Profile;
import com.portofolio.domain.Ruangan;

import lombok.Getter;
import lombok.Setter;
/**
*
* 
*/
@Getter
@Setter
public class UserDto  {

	

	String namaUser;
	String encrypted;
	
	Long kdUser;	
	Long suKdUser;

	Integer kdProfile; 
	Integer kdNegaraProfile;
	Integer kdNegaraPegawai;
	
	
	String kdModulAplikasi;
	String kdLokasi;
	String kdRuangan;

	String kdPegawai; 
	String kdDepartemen; 
	String kdJabatan;
	Integer kdKelompokUser; 
	


	Integer kdBahasa;
	Integer kdVersion;
	
	Jabatan jabatan;
	Departemen departemen;
	Profile profile;
	Ruangan ruangan;
	Ruangan lokasi;
	ModulAplikasi modulAplikasi;
	Pegawai pegawai;
	
	KelompokUser kelompokUser;
	KelompokUser superKelompokUser;
	
	List<Profile> profiles;	
	List<ModulAplikasi> modulAplikasis;	
	List<Ruangan> lokasis;
	List<Ruangan> ruangans;
	

}	
