package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
*
* 
*/
@Getter
@Setter
public class LoginDto {

	@NotNull(message="kode harus Diisi")
	String namaUser;
	
	@NotNull(message="Kata sandi Harus Diisi")
	String kataSandi;

}
