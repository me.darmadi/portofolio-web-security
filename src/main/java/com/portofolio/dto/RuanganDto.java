package com.portofolio.dto;

import lombok.Getter;
import lombok.Setter;

/**
*
* 
*/
@Getter
@Setter
public class RuanganDto {

	String kdRuangan;
	String namaRuangan;
	String kdModulAplikasi;
}
