package com.portofolio.impl;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.Query;
//import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.portofolio.dao.mapper.NotificationMessagingMapper;
import com.portofolio.dao.mapper.SettingDataFixedMapper;
import com.portofolio.domain.NotifMessagingObjekModulAplikasi;
import com.portofolio.domain.ObjekModulAplikasi;
import com.portofolio.domain.Pegawai;
import com.portofolio.domain.Ruangan;
import com.portofolio.dto.SessionDto;
import com.portofolio.notification.MessagePublisher;
import com.portofolio.service.LoginUserService;
import com.portofolio.util.CommonUtil;
import com.portofolio.util.StringUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import io.socket.client.IO;

@Component("messagePublisher")
public class MessagePublisherImpl<K, V> implements MessagePublisher<K, V> {
	private static final Logger LOGGER = LoggerFactory.getLogger(MessagePublisherImpl.class);

	static io.socket.client.Socket socket = null;

//	@PersistenceContext
//	protected EntityManager em;
//
//	@Autowired
//	DataSource dataSource;
	
	@Autowired
	LoginUserService loginUserService;
	
	@Autowired
	SettingDataFixedMapper settingDataFixedMapper;
	
	@Override
	public String GetSettingDataFixed(String namaField) {
		try{
			
			String host = settingDataFixedMapper.getStringSettingDataFixed(loginUserService.getSession().getKdProfile(), namaField);
			return host;
			
//			StringBuffer buffer = new StringBuffer();
//			buffer.append("select model.nilaiField from SettingDataFixed ")
//					.append(" model  where model.id.namaField =:namaField ")
//					.append(" and model.id.kdProfile=:kdProfile")
//					.append(" and model.statusEnabled=true ");
//				
//			Query query = em.createQuery(buffer.toString());
//	
//			query.setParameter("kdProfile", loginUserService.getSession().getKdProfile());
//			query.setParameter("namaField", namaField);	
//			
//			return (String) query.getSingleResult();
		}catch(Exception e){
			StringUtil.printStackTrace(e);
			return "127.0.0.1";
		}
	}

	@Override
	public void sendDirectNotification(final Map<K, V> data) {

		try {
			if (socket == null) {
				socket = IO.socket(GetSettingDataFixed("UrlSocketMessaging"));

				socket.on(io.socket.client.Socket.EVENT_CONNECT, new io.socket.emitter.Emitter.Listener() {

					@Override
					public void call(Object... args) {

						try {
							Gson gson = new Gson();
							JSONObject item = new JSONObject(
									"{\"to\":\"NOTIF\",\"message\":" + gson.toJson(data) + "}");
							LOGGER.info("{\"to\":\"NOTIF\",\"message\":" + gson.toJson(data) + "}");
							socket.emit("subscribe", item);
							// socket.disconnect();
						} catch (JSONException e) {
						}
					}

				});
				socket.connect();
			} else {
				try {
					Gson gson = new Gson();
					JSONObject item = new JSONObject("{\"to\":\"NOTIF\",\"message\":" + gson.toJson(data) + "}");
					LOGGER.info("{\"to\":\"NOTIF\",\"message\":" + gson.toJson(data) + "}");
					socket.emit("subscribe", item);
					// socket.disconnect();
				} catch (JSONException e) {
				}

			}

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			StringUtil.printStackTrace(e);
		}
	}

	static final class DefaultRabbitHole implements MessagePublisher.RabbitHole {
		Connection connection;
		Channel channel;
		String routingKeyAndQueueName;
		Gson gson;
		//String urlSocket;
		String host;
		
		public DefaultRabbitHole(String host){
			this.gson = new Gson();
			this.host = host;
		}

		public void connect(String host, String userName, String password, String routingKeyAndQueueName) throws Exception {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(host);
			factory.setUsername(userName);
			factory.setPassword(password);
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(routingKeyAndQueueName, true, false, false, null);
			this.routingKeyAndQueueName = routingKeyAndQueueName;
		}

		public void sendRabbitMQNotification(String pesan) throws Exception {
			channel.basicPublish("", routingKeyAndQueueName, MessageProperties.PERSISTENT_TEXT_PLAIN, pesan.getBytes());
		}

		public void close() throws Exception {
			channel.close();
			connection.close();
		}
		
		private void resolveObjekModulAplikasi(Integer kdProfile, List<NotifMessagingObjekModulAplikasi> lMessagingObjekModulAplikasi, NotificationMessagingMapper notificationMessagingMapper) {
			for (NotifMessagingObjekModulAplikasi messagingObjekModulAplikasi:lMessagingObjekModulAplikasi) {
				messagingObjekModulAplikasi.setObjekModulAplikasi(
						notificationMessagingMapper.findObjekModulAplikasiByKode(
								messagingObjekModulAplikasi.getKdObjekModulAplikasiTujuan()
						)
					);
				
			}
		}
		
		private void resolveRuangan(Integer kdProfile, List<NotifMessagingObjekModulAplikasi> lMessagingObjekModulAplikasi, NotificationMessagingMapper notificationMessagingMapper) {
			for (NotifMessagingObjekModulAplikasi messagingObjekModulAplikasi:lMessagingObjekModulAplikasi) {
				messagingObjekModulAplikasi.setRuangan(
						notificationMessagingMapper.findRuanganByKode(kdProfile,
								messagingObjekModulAplikasi.getKdRuanganTujuan()
						)
					);
				
			}
		}	
		
		private boolean jabatanTujuan(String kdJabatanTujuan, String kdJabatanHead, Integer hanyaUntukAtasan) {
			if (CommonUtil.isNullOrEmpty(kdJabatanTujuan)) {
				return false;
			} else if (CommonUtil.isNullOrEmpty(hanyaUntukAtasan) || hanyaUntukAtasan.intValue() != 1) {
				return true;
			} else if (CommonUtil.isNullOrEmpty(kdJabatanHead) || !kdJabatanHead.equals(kdJabatanTujuan)) {
				return false;
			} else {
				return true;
			}
		}
				
		public void sendNotif(MessagePublisher.RabbitHole rabbitHole, SessionDto sess, Integer kdProfile, String dariKdRuangan, Pegawai pegawai,
				NotificationMessagingMapper notificationMessagingMapper, 
				List<Integer> kdNotifMessaging, List<String> lKdRuanganTujuan, boolean jabatan) throws Exception {
			
			List<NotifMessagingObjekModulAplikasi> lMessagingObjekModulAplikasi = CommonUtil.createList();
			
			
			
			if (CommonUtil.isNotNullOrEmpty(lKdRuanganTujuan)) {
				List<NotifMessagingObjekModulAplikasi> lMessagingObjekModulAplikasiR = notificationMessagingMapper
						.findByNotifMessagingAndRuangan(sess.getKdProfile(), kdNotifMessaging, lKdRuanganTujuan);
				
				lMessagingObjekModulAplikasi.addAll(lMessagingObjekModulAplikasiR);
			}
			
			if (jabatan) {
				List<NotifMessagingObjekModulAplikasi> lMessagingObjekModulAplikasiJ = notificationMessagingMapper
						.findByNotifMessagingAndJabatan(sess.getKdProfile(), kdNotifMessaging);
				
				
				
				lMessagingObjekModulAplikasi.addAll(lMessagingObjekModulAplikasiJ);
			}
			
			if (CommonUtil.isNullOrEmpty(lMessagingObjekModulAplikasi)) {
				lMessagingObjekModulAplikasi = notificationMessagingMapper
						.findByNotifMessaging(sess.getKdProfile(), kdNotifMessaging);
			}
			

			if (CommonUtil.isNullOrEmpty(lMessagingObjekModulAplikasi)) {
				return;
			}

			String ruanganTujuanIdtemp = "0";
			boolean connect = false;
			
			resolveObjekModulAplikasi(sess.getKdProfile(),lMessagingObjekModulAplikasi, notificationMessagingMapper);
			resolveRuangan(sess.getKdProfile(),lMessagingObjekModulAplikasi, notificationMessagingMapper);
			
			String kdJabatanHead = notificationMessagingMapper.getJabatanAtasan(sess.getKdProfile(), pegawai.getKdJabatan());

			for (NotifMessagingObjekModulAplikasi vo : lMessagingObjekModulAplikasi) {
				String kdRuanganTujuan = vo.getKdRuanganTujuan();
				String kdJabatanTujuan = vo.getKdJabatanTujuan();
				String kdModulAplikasiTujuan = vo.getKdModulAplikasiTujuan();
				String kdObjekModulAplikasiTujuan = vo.getKdObjekModulAplikasiTujuan();
				ObjekModulAplikasi objekModulAplikasiTujuan = vo.getObjekModulAplikasi();
				Ruangan ruangan = vo.getRuangan();
				String customURLObjekModul = vo.getCustomURLObjekModul();
				String titleNotifikasi = vo.getTitleNotifikasi();
				String pesanNotifikasi = vo.getPesanNotifikasi();
				String namaFungsiFrontEnd = vo.getNamaFungsiFrontEnd();

				if ((CommonUtil.isNotNullOrEmpty(kdRuanganTujuan) && kdRuanganTujuan.equals(dariKdRuangan) && lMessagingObjekModulAplikasi.size() > 1)
					|| (CommonUtil.isNullOrEmpty(kdRuanganTujuan) && !jabatanTujuan(kdJabatanTujuan, kdJabatanHead, vo.getHanyaUntukAtasan()))) {
					continue;
				}

				// hati2 bisa duplikasi
				if (!ruanganTujuanIdtemp.equals(kdRuanganTujuan) || jabatanTujuan(kdJabatanTujuan, kdJabatanHead, vo.getHanyaUntukAtasan())) {
					String queuName;
					if (CommonUtil.isNullOrEmpty(kdJabatanTujuan)) {
						queuName = vo.getKdProfile() + "_" + kdRuanganTujuan;
					} else { 
						queuName = vo.getKdProfile() + "_" + kdJabatanTujuan;
					}
					
					if (connect) {
						rabbitHole.close();
					}
					
					rabbitHole.connect(host, "rsab", "rsab", queuName);
					connect = true;
					if (CommonUtil.isNullOrEmpty(kdRuanganTujuan)) {
						kdRuanganTujuan = "-";
					} else {	
						ruanganTujuanIdtemp = kdRuanganTujuan;
					}
				}
				
				Map<String, Object> map = CommonUtil.createMap();
				Map<String, Object> data = CommonUtil.createMap();
				
				//map.put("kdMessaging", kdNotifMessaging);
				data.put("rec", UUID.randomUUID());
				data.put("title", titleNotifikasi);
				data.put("dariRuangan", (CommonUtil.isNullOrEmpty(ruangan))?null:ruangan.getNamaRuangan());
				data.put("kdProfile", kdProfile);
				data.put("kdRuanganTujuan", kdRuanganTujuan);
				data.put("kdJabatanTujuan", kdJabatanTujuan);
				data.put("kdModulAplikasiTujuan", kdModulAplikasiTujuan);
				data.put("kdObjekModulAplikasiTujuan", kdObjekModulAplikasiTujuan);
				data.put("titleNotifikasi", titleNotifikasi);
				data.put("pesanNotifikasi", pesanNotifikasi);
				data.put("fromKdPegawai", (CommonUtil.isNullOrEmpty(pegawai))?null:pegawai.getKdPegawai());
				data.put("fromPegawai", (CommonUtil.isNullOrEmpty(pegawai))?null:pegawai.getNamaLengkap());
				data.put("namaFungsiFrontEnd", namaFungsiFrontEnd);
				data.put("urlForm",CommonUtil.isNullOrEmpty(customURLObjekModul)? objekModulAplikasiTujuan.getAlamatURLFormObjek() : customURLObjekModul);

				map.put("type", 1);
				map.put("data", data);
				
				rabbitHole.sendRabbitMQNotification(gson.toJson(map));
			}
			if (connect) {
				rabbitHole.close();
			}
		}		
	}

	@Override
	public RabbitHole getRabbitHole() {		
		return new DefaultRabbitHole(GetSettingDataFixed("UrlRabbitMQMessaging"));
	}


}
