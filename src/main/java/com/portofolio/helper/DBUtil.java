package com.portofolio.helper;

import java.lang.reflect.Field;

import com.portofolio.notification.Ignore;

public final class DBUtil {
	
	@SuppressWarnings("rawtypes") 
	public static String extractParam(Class c) {
		StringBuilder sb = new StringBuilder();

		String coma = "";
		Class parent = c.getSuperclass();
		
		if (parent != null) {
			String field = extractParam(parent);
			if (!"".equals(field.trim())) {
				sb.append(field).append(",");
			}
		}
		
		for (Field field : c.getDeclaredFields()) {
			
			Ignore ignore = field.getAnnotation(Ignore.class);
			
			if (ignore != null) {
				continue;
			}
			
			sb.append(coma).append("#{").append(field.getName()).append("}");
			
			coma = ",";			
		}
		
		return sb.toString();
	}
	
	@SuppressWarnings("rawtypes") 
	public static String extract(Class c, String tblName) {
		
		StringBuilder sb = new StringBuilder();

		String coma = "";
		Class parent = c.getSuperclass();
		
		if (parent != null) {
			String field = extract(parent, tblName);
			if (!"".equals(field.trim())) {
				sb.append(field).append(",");
			}
		}
		
		for (Field field : c.getDeclaredFields()) {

			if (field.isAnnotationPresent(Ignore.class)) {
				continue;
			}
			
			sb.append(coma).append(tblName).append('.').append(field.getName());
			
//			if (field.isAnnotationPresent(Alias.class)) {
//				sb.append(" as ").append(alias.value());
//			}
			
			coma = ",";			
		}
		
		return sb.toString();
	}
	
}
