package com.portofolio.constant;

public class SecurityConstant {
	
	public static final class PageParameter {
		public static final String LIST_DATA = "listData";
		public static final String TOTAL_PAGES = "totalPages";
		public static final String TOTAL_ELEMENTS = "totalElements";
	}

	public static final class HttpHeaderInfo {
		public static final String TOTAL_PAGE_HEADER = "Total-Pages";
		public static final String TOTAL_COUNT_HEADER = "Total-Count";
		public static final String ERROR_MESSAGE = "Error-Message";
		public static final String LABEL_SUCCESS = "SUCCESS";
		public static final String LABEL_ERROR = "label-error";
		public static final String LABEL_SUCCESS_CREATED = "label-success-created";
		public static final String LABEL_SUCCESS_OK = "label-success-ok";
	}

	public static final class MessageInfo {
		public static final String INFO_MESSAGE = "INFO_MESSAGE";
		public static final String WARNING_MESSAGE = "WARNING_MESSAGE";
		public static final String ERROR_MESSAGE = "ERROR_MESSAGE";
		public static final String EXCEPTION_MESSAGE = "EXCEPTION_MESSAGE";

	}


	public static final class HttpHeader {
		public static final String SUPERVISING = "Supervising";
		public static final String MODULE = "Module";
		public static final String FORM = "Form";
		public static final String ACTION = "Action";

		public static final String URL_FORM = "AlamatUrlForm"; // syamsu
		public static final String KD_RUANGAN = "KdRuangan"; // syamsu
		public static final String KD_RUANGAN_T = "KdRuanganT"; // syamsu
		public static final String CR_JABATAN = "CrJabatanT"; // syamsu
		public static final String KD_RUANGAN_A = "KdRuanganA"; // syamsu
		public static final String TGL_KIRIM = "tglKirim"; // syamsu
		// public static final String RUANGAN_TUJUAN = "ruanganTujuan"; //
		// syamsu
		// public static final String ID_RUANGAN_TUJUAN_ALT =
		// "ruanganTujuanAlt"; // syamsu
		public static final String KD_USER = "KdUser"; // syamsu

	}

	// HEADER KEY TOKEN
	public static final String HEADER_AUTHORIZATION = "Authorization";
	public static final String HEADER_X_AUTH_TOKEN = "x-auth-token";

	public static final String MESSAGE = "message";

	public static final String STATUS_CODE = "statusCode";

	public static final String STATUS = "status";

	public static final String SCM = "SkFTQU1FRElLQQ==";

}
