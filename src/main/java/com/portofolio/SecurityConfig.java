package com.portofolio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.google.common.collect.ImmutableList;
import com.portofolio.constant.SecurityConstant;
import com.portofolio.security.RestAuthenticationEntryPoint;
import com.portofolio.security.StatelessAuthenticationFilter;
import com.portofolio.security.TokenAuthenticationService;
import com.portofolio.security.UserService;

/**
 * SpringSecurityConfig class Di sini Kita tidak menggunakan XML Config untuk
 * Spring Security
 * 
 * @author Adik & Syamsu
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

	private final UserService userService;

	private final TokenAuthenticationService tokenAuthenticationService;
		
	@Bean
	public MethodInvokingFactoryBean methodInvokingFactoryBean() {
	    MethodInvokingFactoryBean methodInvokingFactoryBean = new MethodInvokingFactoryBean();
	    methodInvokingFactoryBean.setTargetClass(SecurityContextHolder.class);
	    methodInvokingFactoryBean.setTargetMethod("setStrategyName");
	    methodInvokingFactoryBean.setArguments(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
	    return methodInvokingFactoryBean;
	}

	public SecurityConfig() {
		super(true);
		SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
		this.userService = new UserService();
		tokenAuthenticationService = new TokenAuthenticationService(SecurityConstant.SCM, userService);
		SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
	}

	String[] SpringCloudUrl = {
			"/env",
			"/logging", 
			"/info", 
			"/health", 
			"/error", 
			"/metrics", 
			"/jmx", 
			"/threads", 
			"/trace",
			"/circuitBreaker.stream", 
			"/circuitBreaker**/**", 
			"/hystrix.stream", 
			"/hystrix**/**"
	};
	
	String[] StaticUrl = { 
			"/",
			"/favicon.ico", 
			"/**/*.html", 
			"/**/*.css", 
			"/**/*.js", 
			"/HistoriAbsensi/**",
			"/auth/**", 
			"/api-docs.json",
			"/v2/api-docs/**", 
			"/webjars/**", 
			"/swagger-ui.html", 
			"/swagger-resources/**", 
			"/details/**", 
			"/api/**", 
			"/file/**",
			"/image/**",
			"/video/**",
			"/wizard/**",
			"/**/*.pdf" };

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http
		.cors()
		.and()
		.csrf().disable()
		.anonymous()
		.and()
		.servletApi()
		.and()
		.authorizeRequests()
		.antMatchers(HttpMethod.OPTIONS).permitAll()
		.antMatchers(SpringCloudUrl).permitAll()
		.antMatchers(StaticUrl).permitAll()
		.anyRequest().authenticated()
		.and()
		.addFilterBefore(new StatelessAuthenticationFilter(tokenAuthenticationService),
						UsernamePasswordAuthenticationFilter.class)
				.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint);
		
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		
		config.setAllowCredentials(true);
		config.setAllowedOrigins(ImmutableList.of("*"));
		config.setAllowedHeaders(
				ImmutableList.of(SecurityConstant.HEADER_AUTHORIZATION,
						SecurityConstant.HEADER_X_AUTH_TOKEN,
						"remember-me",
						"content-type",
//						SecurityConstant.HttpHeader.KD_RUANGAN,
						SecurityConstant.HttpHeader.KD_RUANGAN_T,
						SecurityConstant.HttpHeader.KD_RUANGAN_A,
						SecurityConstant.HttpHeader.CR_JABATAN,
						SecurityConstant.HttpHeader.SUPERVISING,
						SecurityConstant.HttpHeader.URL_FORM,
						SecurityConstant.HttpHeader.TGL_KIRIM)
		);
		config.setAllowedMethods(ImmutableList.of("OPTIONS","POST","GET","PUT","DELETE","PATCH"));
		source.registerCorsConfiguration("/**", config);
		
		 return source;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService()).passwordEncoder(new BCryptPasswordEncoder());
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	@Override
	public UserService userDetailsService() {
		return userService;
	}

	@Bean
	public TokenAuthenticationService tokenAuthenticationService() {
		return tokenAuthenticationService;
	}
}

//.and()
// .headers()
// .addHeaderWriter(new
// StaticHeadersWriter("Access-Control-Allow-Headers",allow))
// .addHeaderWriter(new
// StaticHeadersWriter("Access-Control-Expose-Headers",allow))
// .addHeaderWriter(new
// StaticHeadersWriter("Access-Control-Request-Headers",allow))
// .addHeaderWriter(new StaticHeadersWriter("Access-Control-Max-Age","3600"))
// .addHeaderWriter(new
// StaticHeadersWriter("Access-Control-Allow-Methods","POST, GET, PUT, DELETE,
// OPTIONS"))
// .addHeaderWriter(new
// StaticHeadersWriter("Access-Control-Allow-Methods","Access-Control-Allow-Credentials"))
// .headers()
// .cacheControl().disable()


// http.exceptionHandling()
// //restAuthenticationEntryPoint
// .authenticationEntryPoint(restAuthenticationEntryPoint)
//
// .and()
// .anonymous()
// .and()
// .servletApi()
// .and()
// .headers()
// .cacheControl()
// .and()
// .authorizeRequests()
//
//
// // Allow anonymous resource requests
// .antMatchers("/favicon.ico")
// .permitAll()
// .antMatchers("**/*.html")
// .permitAll()
// .antMatchers("**/*.css")
// .permitAll()
// .antMatchers("**/*.js")
// .permitAll()
//
//
// // Allow anonymous logins
// .antMatchers("/auth/**")
// .permitAll()
// .antMatchers("/api-docs.json")
// .permitAll()
//
// .antMatchers("/v2/api-docs/**")
// .permitAll()
//
// .antMatchers("/webjars/**")
// .permitAll()
//
// .antMatchers("/webjars/**")
// .permitAll()
//
// .antMatchers(SpringCloudUrl)
// .permitAll()
//
// .antMatchers("/swagger-ui.html")
// .permitAll()
//
// .antMatchers("/swagger-resources/**")
// .permitAll()
//
//
// // All other request need to be authenticated
// .anyRequest()
// .authenticated()
// .and()
//
//
// // Custom Token based authentication based on the header
// // previously given to the client
// .addFilterBefore(
// new StatelessAuthenticationFilter(
// tokenAuthenticationService),
// UsernamePasswordAuthenticationFilter.class);
// //OPTION
// http.csrf().disable()
// .authorizeRequests()
// .antMatchers(HttpMethod.OPTIONS, "/**").permitAll();


//config.setAllowCredentials(true);
//config.addAllowedOrigin("*");
//config.addAllowedHeader("*");
//config.addAllowedMethod("OPTIONS");
//config.addAllowedMethod("GET");
//config.addAllowedMethod("POST");
//config.addAllowedMethod("PUT");
//config.addAllowedMethod("DELETE");
//config.addAllowedHeader(SecurityConstant.AUTH_HEADER_NAME1);
//config.addAllowedHeader(SecurityConstant.AUTH_HEADER_NAME2);
//config.addAllowedHeader("content-type");
//config.addAllowedHeader("origin");
//config.addAllowedHeader("remember-me");

// CorsConfiguration configuration = new CorsConfiguration();
//
// configuration.setAllowCredentials(true);
// configuration.setAllowedOrigins(ImmutableList.of("*"));
// configuration.setAllowedMethods(ImmutableList.of("GET, POST, PATCH, PUT,
// DELETE, OPTIONS"));
// configuration.setAllowedHeaders(ImmutableList.of(SecurityConstant.AUTH_HEADER_NAME,
// "cache-control",
// "content-type",
// "origin",
// "remember-me"));
// configuration.setExposedHeaders(ImmutableList.of(SecurityConstant.AUTH_HEADER_NAME,
// "cache-control",
// "content-type",
// "origin",
// "remember-me"));
//
// final UrlBasedCorsConfigurationSource source = new
// UrlBasedCorsConfigurationSource();
// source.registerCorsConfiguration("/**", configuration);
//

