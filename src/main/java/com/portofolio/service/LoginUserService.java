package com.portofolio.service;

import com.portofolio.domain.Pegawai;
import com.portofolio.domain.Profile;
import com.portofolio.dto.SessionDto;
import com.portofolio.dto.UserDto;



public interface LoginUserService extends AuthService {
	
	UserDto getUser();
	Pegawai getPegawai(SessionDto sessionDto);
	Profile getProfile(SessionDto sessionDto);
		
	
}
