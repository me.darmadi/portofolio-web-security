package com.portofolio.service;

import com.portofolio.domain.ObjekModulAplikasi;

public interface RuleObjekModulAplikasiService {

	ObjekModulAplikasi findByAlamatUrlForm(String alamatUrlForm);
	
}
