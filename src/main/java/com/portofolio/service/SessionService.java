package com.portofolio.service;

import com.portofolio.dto.AuthDto;
import com.portofolio.dto.SessionDto;

public interface SessionService {
	SessionDto getSession();
	SessionDto toSessionDto(String json);
	String toSessionJson(AuthDto authDto);
}
