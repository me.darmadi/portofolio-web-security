package com.portofolio.service;

import com.portofolio.dto.PermissionDto;

public interface PermissionService {

	PermissionDto getPermission(String kdObjectModulAplikasi, Integer kdKelompokUser, String kdModulAplikasi, Integer kdProfile);
	
}
