package com.portofolio.service;

import java.util.List;

import com.portofolio.domain.NotifMessaging;


public interface MessagingService {

//	List<NotifMessaging> findMessagingByUrlOrObjekModulAplikasi(String kdObjekModulAplikasi, String uRI);
	List<NotifMessaging> findMessagingByObjekModulAplikasi(String kdObjekModulAplikasi);

}
