package com.portofolio.service;

import java.util.List;

import com.portofolio.domain.NotifMessagingObjekModulAplikasi;

public interface MessageObjekModulService {

	List<NotifMessagingObjekModulAplikasi> findByMessagingAndRuangan(Integer kdNotifMessaging, List<String> lKdRuanganTujuan);
}
