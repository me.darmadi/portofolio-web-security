package com.portofolio.service;

import com.portofolio.domain.Pasien;
import com.portofolio.dto.PasienDto;
import com.portofolio.dto.SessionDto;


public interface LoginPasienService extends AuthService {

	PasienDto getPasien();
	Pasien getPasien(SessionDto sessionDto);
	
}
