package com.portofolio.service;

import java.util.Map;

import com.portofolio.dto.AuthDto;
import com.portofolio.dto.LoginDto;
import com.portofolio.dto.SessionDto;

public interface AuthService extends SessionService {

	AuthDto listRuangan(AuthDto authDto);
	AuthDto listLokasi(AuthDto authDto);
	AuthDto listModulAplikasi(AuthDto authDto);
	AuthDto signIn(LoginDto loginDto);
	AuthDto buildAuthDtoFromSession(SessionDto sessDto);
	AuthDto authorize(AuthDto authDto);
	
	Map<String,Object> resultRuangan(AuthDto authDto);
	Map<String,Object> resultLokasi(AuthDto authDto);
	Map<String,Object> resultProfile(AuthDto authDto);
	Map<String,Object> resultModulAplikasi(AuthDto authDto);
	Map<String, Object> getResult(AuthDto authDto);
	

}
