package com.portofolio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.portofolio.dao.mapper.NotificationMessagingMapper;
import com.portofolio.domain.NotifMessaging;
import com.portofolio.dto.SessionDto;
import com.portofolio.service.LoginUserService;
import com.portofolio.service.MessagingService;

@Service("MessagingService")
public class MessagingServiceImpl implements MessagingService {
	
	@Autowired
	NotificationMessagingMapper messagingMapper;
	
	@Autowired
	LoginUserService loginUserService;

//	@Override
//	public NotifMessaging findMessagingByUrlOrObjekModulAplikasi(String kdObjekModulAplikasi, String uRI) {		
//		return messagingMapper.findMessagingByUrlOrObjekModulAplikasi(kdObjekModulAplikasi, uRI);
//	}
	
	@Override
	public List<NotifMessaging> findMessagingByObjekModulAplikasi(String kdObjekModulAplikasi) {		
		SessionDto sess =  loginUserService.getSession();
		return messagingMapper.findNotifMessagingByObjekModulAplikasi(sess.getKdProfile(), sess.getKdModulAplikasi(), kdObjekModulAplikasi);
	}

}
