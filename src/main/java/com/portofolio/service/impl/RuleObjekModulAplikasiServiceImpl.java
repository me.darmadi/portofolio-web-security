package com.portofolio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.portofolio.dao.mapper.AppPermissionMapper;
import com.portofolio.domain.ObjekModulAplikasi;
import com.portofolio.service.RuleObjekModulAplikasiService;

@Service("RuleObjekModulAplikasiService")
public class RuleObjekModulAplikasiServiceImpl implements RuleObjekModulAplikasiService {

	@Autowired
	AppPermissionMapper permission;
	
	@Override
	public ObjekModulAplikasi findByAlamatUrlForm(String alamatURLFormObjek) {
		ObjekModulAplikasi objekModulAplikasi = null;
		objekModulAplikasi = permission.findByAlamatUrlForm(alamatURLFormObjek);
		return objekModulAplikasi;
	}

}
