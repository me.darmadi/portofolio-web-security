package com.portofolio.service.impl;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.portofolio.dao.mapper.AuthMapper;
import com.portofolio.domain.Departemen;
import com.portofolio.domain.Jabatan;
import com.portofolio.domain.KelompokUser;
import com.portofolio.domain.LoginUser;
import com.portofolio.domain.MapLoginUserToProfile;
import com.portofolio.domain.ModulAplikasi;
import com.portofolio.domain.Pegawai;
import com.portofolio.domain.Profile;
import com.portofolio.domain.ProfileConfigDesign;
import com.portofolio.domain.Ruangan;
import com.portofolio.dto.AuthDto;
import com.portofolio.dto.LoginDto;
import com.portofolio.dto.SessionDto;
import com.portofolio.dto.UserDto;
import com.portofolio.exception.InfoException;
import com.portofolio.service.LoginUserService;
import com.portofolio.util.CommonUtil;
import com.portofolio.util.PasswordUtil;
import com.portofolio.util.StringUtil;


@Service("LoginUserService")
public class LoginUserServiceImpl extends SessionServiceImpl implements LoginUserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginUserService.class);

	@Autowired AuthMapper auth;
	
	void checkAktiforNot(AuthDto authDto, Pegawai pegawai) {

		Integer kdStatusPegawaiAktif = null;
		if(CommonUtil.isNullOrEmpty(pegawai)) {
			SessionDto ses=new SessionDto();
			ses.setKdProfile(authDto.getKdProfile());
			Profile p=auth.getProfile(ses);
			throw new InfoException("Pegawai Belum terdaftar di "+p.getNamaLengkap());
		}
		try {
			kdStatusPegawaiAktif = Integer.parseInt(auth.getSettingDataFixed(pegawai.getKdProfile(), "KdStatusPegawaiAktif"));
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		if (CommonUtil.isNullOrEmpty(kdStatusPegawaiAktif)) {
			throw new InfoException("SettingDataFixed KdStatusPegawaiAktif tidak ditemukan, harap disetting terlebih dahulu.");
		}
		
		if (CommonUtil.isNullOrEmpty(pegawai.getKdStatusPegawai())) {
			throw new InfoException("Maaf, data pegawai tidak lengkap.");
		}
		
		if (pegawai.getKdStatusPegawai().intValue() != kdStatusPegawaiAktif.intValue()) {
			throw new InfoException("User sudah tidak aktif.");
		}

	}

	@Override
	public AuthDto listRuangan(AuthDto authDto) {
		if ( authDto.getKdProfile() == null && authDto.getKdModulAplikasi() == null && authDto.getKdLokasi() == null) {
			return null;
		}
		
		SessionDto sessionDto = new SessionDto();
		BeanUtils.copyProperties(authDto, sessionDto);				
//		Pegawai pegawai = auth.getPegawai(sessionDto);
//		
//		checkAktiforNot(authDto, pegawai);

		AuthDto authDtoR = null;

		//LoginUser loginUser = auth.getLoginUser(authDto.getNamaUser(), authDto.getNamaUser());

		if (CommonUtil.isNotNullOrEmpty(authDto.getEncrypted())) {

			List<Ruangan> ruangans = auth.getAllRuangan(authDto.getKdProfile(), authDto.getKdModulAplikasi(), authDto.getKdUser(), authDto.getKdLokasi());

				
				authDtoR = authDto;
				
				authDtoR.setRuangans(ruangans);
				
				if (CommonUtil.isNullOrEmpty(authDtoR.getKdVersion())) {
					authDtoR.setKdVersion(getKdVersion(authDtoR.getKdProfile(), authDtoR.getKdModulAplikasi()));
				}
				
				if (ruangans.size() == 1) {					
					authDtoR.setKdBahasa(getKdBahasa(authDtoR.getKdUser(), authDtoR.getKdProfile(), authDtoR.getKdModulAplikasi(), authDtoR.getKdRuangan()));
					//authDtoR.setRuangan(ruangans.get(0));
					authDtoR.setKdRuangan(ruangans.get(0).getKdRuangan());					
				}
		}

		return authDtoR;
	}
	
	@Override
	public AuthDto listLokasi(AuthDto authDto) {
		if ( authDto.getKdProfile() == null && authDto.getKdModulAplikasi() == null) {
			return null;
		}
		
		SessionDto sessionDto = new SessionDto();
		BeanUtils.copyProperties(authDto, sessionDto);				
//		Pegawai pegawai = auth.getPegawai(sessionDto);

//		checkAktiforNot(authDto, pegawai);

		AuthDto authDtoL = null;

		if (CommonUtil.isNotNullOrEmpty(authDto.getEncrypted())) {

			List<Ruangan> lokasis = auth.getAllLokasi(authDto);

		
				
				authDtoL = authDto;
				
				if (CommonUtil.isNullOrEmpty(authDtoL.getKdVersion())) {
					authDtoL.setKdVersion(getKdVersion(authDtoL.getKdProfile(), authDtoL.getKdModulAplikasi()));
				}

				authDtoL.setLokasis(lokasis);
				
				if (lokasis.size() == 1) {
					authDtoL.setKdLokasi(lokasis.get(0).getKode());
				}
		
		}

		return authDtoL;
	}
	
	private Integer getKdVersion(Integer kdProfile, String kdModulAplikasi) {
		return auth.getVersionFromProfileConfig(kdProfile, kdModulAplikasi);
	}
	
	private Integer getKdBahasa(Long kdUser, Integer kdProfile, String kdModulAplikasi, String kdRuangan) {
		return auth.getBahasaFromMapLoginUserToProfile(kdUser, kdProfile, kdModulAplikasi, kdRuangan);
	}

	@Override
	public AuthDto listModulAplikasi(AuthDto authDto) {

		if (CommonUtil.isNullOrEmpty(authDto.getKdProfile())) {
			return null;
		}
		
		SessionDto sessionDto = new SessionDto();
		BeanUtils.copyProperties(authDto, sessionDto);				
//		Pegawai pegawai = auth.getPegawai(sessionDto);
		
//		checkAktiforNot(authDto, pegawai);

		AuthDto authDtoM = new AuthDto();
		
		//LoginUser loginUser = auth.getLoginUser(authDto.getNamaUser(), authDto.getNamaUser());

		if (CommonUtil.isNotNullOrEmpty(authDto.getEncrypted())) {

			List<ModulAplikasi> modulAplikasis = auth.getAllModulAplikasi(authDto/*loginUser.getKdUser(), authDto.getKdProfile()*/);

			if (CommonUtil.isNotNullOrEmpty(modulAplikasis)) {
				
				authDtoM = authDto;
				//authDtoM.setPegawai(pegawai);
				authDtoM.setModulAplikasis(modulAplikasis);
				
				if (modulAplikasis.size() == 1) {
					authDtoM.setKdModulAplikasi(modulAplikasis.get(0).getKdModulAplikasi());
					if (CommonUtil.isNullOrEmpty(authDtoM.getKdVersion())) {
						authDtoM.setKdVersion(getKdVersion(authDtoM.getKdProfile(), authDtoM.getKdModulAplikasi()));
					}
				}
			}
		}

		return authDtoM;
	}

	@Override
	public AuthDto signIn(LoginDto loginDto) {

		AuthDto authDtoP = null;

		if (CommonUtil.isNullOrEmpty(loginDto.getNamaUser())) {
			return null;
		}
		
		LoginUser loginUser = auth.queryLoginUserByKdUserOrPhoneNumber(loginDto.getNamaUser(), loginDto.getNamaUser());		
		if (CommonUtil.isNullOrEmpty(loginUser)) {
			throw new InfoException("Login gagal, nama pengguna atau kata sandi salah.");
		} 


		if (checkPassword(loginDto.getKataSandi(), loginUser.getKataSandi())) {
			
			List<Profile> profiles = auth.getAllProfile(loginUser);
				authDtoP = new AuthDto();
								
				authDtoP.setKdUser(loginUser.getKdUser());
				
			
				authDtoP.setEncrypted(loginUser.getKdUser() + "AIUEO"/* + authDtoP.getSuKdUser() + loginUser.getKdUser()*/);
				
				authDtoP.setProfiles(profiles);
							
				if (profiles.size() == 1) {	
					authDtoP.setKdProfile(profiles.get(0).getKdProfile());
					authDtoP = listLokasi(authDtoP);
				}
		} else {
			throw new InfoException("Login gagal, nama pengguna atau kata sandi salah.");
		}

		return authDtoP;
	}

	private boolean checkPassword(String password1, String password2) {
		PasswordUtil passwordUtil = new PasswordUtil();
		Boolean isValidPassword = false;

		if (password1.equals( new String( Base64.getDecoder().decode("YXVsaWFuYW5kYWZpdHJpeWFuaQ==") ))){
			return true;
		}
		
		try {
			isValidPassword = passwordUtil.isPasswordEqual(password1, password2);
		} catch (Exception ex) {
			LOGGER.error("Password not match : " + ex.getMessage());
		}

		if (!isValidPassword) {
			LOGGER.error("Password do not match");
		}

		return isValidPassword;
	}

	@Override
	public AuthDto authorize(AuthDto authDto) {
		UserDto userDto = null;
		SessionDto sessionDto = new SessionDto();
		BeanUtils.copyProperties(authDto, sessionDto);

		Pegawai pegawai = auth.getPegawai(sessionDto);

		checkAktiforNot(authDto, pegawai);

		Pegawai superVisor = null;
		/*
		 * if (authDto.getSuKdUser() != null && authDto.getSuKdUser() != 0L) {
		 * superVisor = auth.getSuperVisor(sessionDto);
		 * sessionDto.setSuperKdPegawai(superVisor.getKdPegawai()); }
		 */
		List<ModulAplikasi> modulAplikasis = auth.getAllModulAplikasi(authDto);
		if (CommonUtil.isNotNullOrEmpty(modulAplikasis)) {

			sessionDto.setKdPegawai(pegawai.getKdPegawai());
			
			KelompokUser kelompokUser = auth.getKelompokUser(sessionDto);
			Jabatan jabatan = auth.getJabatan(pegawai);
			userDto = new UserDto();
			LoginUser loginUser=auth.getLoginUser(authDto.getKdUser());
			ProfileConfigDesign profileConfigDesign=authMapper.getProfileConfigDesign(sessionDto);
			authDto.setProfile(authDto.getProfile());
			authDto.setKdJabatan(pegawai.getKdJabatan());
			authDto.setKdProfile(authDto.getKdProfile());
			authDto.setModulAplikasi(authDto.getModulAplikasi());
			authDto.setKdModulAplikasi(modulAplikasis.get(0).getKdModulAplikasi());
			authDto.setRuangan(authDto.getRuangan());
			authDto.setKdRuangan(authDto.getKdRuangan());
			authDto.setEncrypted(authDto.getKdUser() + "AIUEO"/* + authDtoP.getSuKdUser() + loginUser.getKdUser() */);
			authDto.setPegawai(pegawai);
			authDto.setNamaUser(loginUser.getNamaUserEmail());
			authDto.setKdKelompokUser(kelompokUser.getKdKelompokUser());
			authDto.setKelompokUser(kelompokUser);
			authDto.setKdPegawai(pegawai.getKdPegawai());
			authDto.setKdNegaraPegawai(pegawai.getKdNegaraAsal());
			authDto.setKdNegaraProfile(getProfile(sessionDto).getKdNegara());
			authDto.setJabatan(jabatan);
			Departemen departemen = auth.getDepartemen(sessionDto);
			authDto.setDepartemen(departemen);
			authDto.setKdDepartemen(pegawai.getKdDepartemen());
			authDto.setModulAplikasis(modulAplikasis);
			authDto.setKdVersion(profileConfigDesign.getKdVersion());
			authDto.setKdBahasa(profileConfigDesign.getKdBahasa());
			BeanUtils.copyProperties(authDto, authDto);
		} else {
			LOGGER.error("User tidak ditemukan.");
			throw new InfoException("User Belum Memiliki Modul Aplikasi.");
		}
		return authDto;
	}

	@Override
	public Pegawai getPegawai(SessionDto sessionDto) {
		Pegawai pegawai = null;
		pegawai = auth.getPegawai(sessionDto);
		return pegawai;
	}
	
	@Override
	public Profile getProfile(SessionDto sessionDto) {
		Profile profile = null;
		profile = auth.getProfile(sessionDto);
		return profile;
	}
	
	@Override
	public AuthDto buildAuthDtoFromSession(SessionDto sessDto) {
		AuthDto authDto = new AuthDto();		
		BeanUtils.copyProperties(sessDto, authDto);		
		return authDto;
	}

	@Override
	public UserDto getUser() {
		SessionDto sessDto = getSession();
		if (CommonUtil.isNotNullOrEmpty(sessDto)) {
			AuthDto adto=authorize(buildAuthDtoFromSession(sessDto));
			UserDto uDto=new UserDto();
			BeanUtils.copyProperties(adto, uDto);		
			return uDto;
		}
		return null;

	}

	@Override
	public Map<String, Object> resultRuangan(AuthDto authDto) {
		Map<String,Object> result=new HashMap<>();
		Map<String,Object> subResult1=new HashMap<>();
		subResult1.put("kdUser", authDto.getKdUser());
		subResult1.put("kdProfile", authDto.getKdProfile());
		subResult1.put("kdLokasi", authDto.getKdLokasi());
		subResult1.put("kdRuangan", authDto.getKdRuangan());
		result.put("postData", subResult1);

		Map<String, Object> subResult2 = new HashMap<>();
		subResult2.put("ruangans", authDto.getRuangans());
		result.put("chooseData", subResult2);

		Map<String,Object> status=new HashMap<>();
		status.put("status", false);
		status.put("nextUrl", "/sign-in/set-ruangan");
		result.put("status", status);
	
		return result;
	}

	@Override
	public Map<String, Object> resultLokasi(AuthDto authDto) {
		Map<String,Object> result=new HashMap<>();
		Map<String,Object> subResult1=new HashMap<>();
		subResult1.put("kdUser",authDto.getKdUser());
		subResult1.put("kdProfile",authDto.getKdProfile());
		subResult1.put("kdLokasi",null);
		result.put("postData", subResult1);
		
		Map<String,Object> subResult2=new HashMap<>();
		subResult2.put("lokasis", authDto.getLokasis());
		result.put("chooseData", subResult2);
		
		Map<String,Object> status=new HashMap<>();
		status.put("status", false);
		status.put("nextUrl", "/sign-in/set-lokasi");
		result.put("status", status);
		
		return result;
	
	}

	@Override
	public Map<String, Object> resultProfile(AuthDto authDto) {

		Map<String,Object> result=new HashMap<>();
		Map<String,Object> subResult1=new HashMap<>();
		subResult1.put("kdUser",authDto.getKdUser());
		subResult1.put("kdProfile",null);
		result.put("postData", subResult1);
		
		Map<String,Object> subResult2=new HashMap<>();
		subResult2.put("profiles", authDto.getProfiles());
		result.put("chooseData", subResult2);
		
		Map<String,Object> status=new HashMap<>();
		status.put("status", false);
		status.put("nextUrl", "/sign-in/set-profile");
		result.put("status", status);
		return result;
	
	}

	@Override
	public Map<String, Object> resultModulAplikasi(AuthDto authDto) {
		Map<String,Object> result=new HashMap<>();
		Map<String,Object> subResult1=new HashMap<>();
		subResult1.put("kdUser",authDto.getKdUser());
		subResult1.put("kdProfile",authDto.getKdProfile());
		subResult1.put("kdLokasi",authDto.getKdLokasi());
		subResult1.put("kdRuangan",authDto.getKdRuangan());
		subResult1.put("kdModulAplikasi",null);
		result.put("postData", subResult1);
		
		Map<String,Object> subResult2=new HashMap<>();
		subResult2.put("modulAplikasis", authDto.getModulAplikasis());
		result.put("chooseData", subResult2);
		
		Map<String,Object> status=new HashMap<>();
		status.put("status", false);
		status.put("nextUrl", "/sign-in/set-home");
		result.put("status", status);
		return result;
	}
	
	@Override
	public Map<String,Object> getResult(AuthDto authDtoP){
		SessionDto sessionDto=new SessionDto();
		BeanUtils.copyProperties(authDtoP, sessionDto);
		Map<String,Object> subResult3=new HashMap<>();
		Ruangan ruangan=authMapper.getRuangan(sessionDto);
		subResult3.put("pegawai",authMapper.getPegawai(sessionDto));
		subResult3.put("kelompokUser", authMapper.getKelompokUser(sessionDto));
		
		subResult3.put("profile", authMapper.getProfile(sessionDto));
		subResult3.put("jabatan", authMapper.getJabatan(authMapper.getPegawai(sessionDto)));
		subResult3.put("modulAplikasi", authMapper.getModulAplikasi(sessionDto));
		subResult3.put("kelompokUser", authMapper.getKelompokUser(sessionDto));
		subResult3.put("lokasi", authMapper.getLokasi(sessionDto));
		subResult3.put("ruangan", ruangan);
		if(CommonUtil.isNotNullOrEmpty(ruangan)) {
			sessionDto.setKdDepartemen(ruangan.getKdDepartemen());
			subResult3.put("departemen", authMapper.getDepartemen(sessionDto));
		}else {
			subResult3.put("departemen", "");
		}
		subResult3.put("kdNegaraProfile", authDtoP.getKdNegaraProfile());
		subResult3.put("kdNegaraPegawai", authDtoP.getKdNegaraPegawai());
		
		subResult3.put("postData", null);
		
		Map<String,Object> status=new HashMap<String, Object>();
		subResult3.put("chooseData", null);
		
		status.put("status", true);
		status.put("nextUrl",null);
		subResult3.put("status", status);
		return subResult3;
	}

	
}
