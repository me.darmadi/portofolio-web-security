package com.portofolio.service.impl;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.portofolio.dao.mapper.AuthMapper;
import com.portofolio.domain.Pegawai;
import com.portofolio.dto.AuthDto;
import com.portofolio.dto.SessionDto;
import com.portofolio.exception.InfoException;
import com.portofolio.service.SessionService;
import com.portofolio.util.StringUtil;

@Service("SessionService")
public class SessionServiceImpl implements SessionService {
	
	//private final Logger LOG = LoggerFactory.getLogger(SessionService.class);
	
	@Autowired
	AuthMapper authMapper;
	
	@Override
	public SessionDto getSession() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return toSessionDto(principal.toString());
	}

	@Override
	public SessionDto toSessionDto(String json) {
		try {
			JSONObject sessJson = new JSONObject(json);
			SessionDto ses=new SessionDto();
			ses.setKdPegawai(sessJson.optString("kdPegawai"));
			ses.setKdProfile(sessJson.optInt("kdProfile"));
				Pegawai p=authMapper.getPegawai(ses);
				SessionDto sessDto = new SessionDto();
				sessDto.setKdUser(sessJson.optLong("kdUser"));
				sessDto.setNamaUser(sessJson.optString("user"));
				sessDto.setEncrypted(sessJson.optString("encrypted"));
				sessDto.setKdProfile(sessJson.optInt("kdProfile"));
				sessDto.setKdModulAplikasi(sessJson.optString("kdModulAplikasi"));
				sessDto.setKdRuangan(sessJson.optString("kdRuangan"));
				sessDto.setKdLokasi(sessJson.optString("kdLokasi"));
				sessDto.setKdPegawai(sessJson.optString("kdPegawai"));
				sessDto.setKdDepartemen(sessJson.optString("kdDepartemen"));
				sessDto.setKdJabatan(sessJson.optString("kdJabatan"));
				sessDto.setKdKelompokUser(sessJson.optInt("kdKelompokUser"));
				sessDto.setKdBahasa(sessJson.optInt("kdBahasa"));
				sessDto.setKdVersion(sessJson.optInt("kdVersion"));

				sessDto.setKdNegaraPegawai(sessJson.optInt("kdNegaraPegawai"));
				sessDto.setKdNegaraProfile(sessJson.optInt("kdNegaraProfile"));
				sessDto.setNamaUser(sessJson.optString("namaUser"));
				
				sessDto.setInfo(sessJson.optString("info"));
	
	
				return sessDto;
			
	
		}catch(Exception e) {
			//LOG.warn("Error pada sessionnya, cek Token JWTnya. -> " + e.getMessage() );
			throw new InfoException("Session invalid, silahkan login ulang..");
		}
	}
	
	@Override
	public String toSessionJson(AuthDto authDto) {
		JSONObject sessJson = new JSONObject();
			try {
				sessJson.put("kdUser", authDto.getKdUser());
				sessJson.put("encrypted", authDto.getEncrypted());
				sessJson.put("kdProfile", authDto.getKdProfile());
				sessJson.put("kdModulAplikasi", authDto.getKdModulAplikasi());
				sessJson.put("kdRuangan", authDto.getKdRuangan());
				sessJson.put("kdLokasi", authDto.getKdLokasi());
				sessJson.put("kdPegawai", authDto.getKdPegawai());
				sessJson.put("kdDepartemen", authDto.getKdDepartemen());
				sessJson.put("kdKelompokUser", authDto.getKdKelompokUser());
				sessJson.put("kdNegaraProfile", authDto.getKdNegaraProfile());
				sessJson.put("kdNegaraPegawai", authDto.getKdNegaraPegawai());
				sessJson.put("kdBahasa", authDto.getKdBahasa());
				sessJson.put("kdJabatan", authDto.getKdJabatan());
				sessJson.put("kdVersion", authDto.getKdVersion());
				

				
			} catch (Exception e) {
				StringUtil.printStackTrace(e);
			}
		
		String sessString = sessJson.toString();
		return sessString;
	}
}
