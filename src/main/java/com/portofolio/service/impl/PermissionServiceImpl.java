package com.portofolio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.portofolio.dao.mapper.AppPermissionMapper;
import com.portofolio.domain.MapObjekModulToKelompokUser;
import com.portofolio.dto.PermissionDto;
import com.portofolio.service.PermissionService;
import com.portofolio.util.CommonUtil;
import com.portofolio.util.StringUtil;

@Service("PermissionService")
public class PermissionServiceImpl implements PermissionService {

	@Autowired
	AppPermissionMapper permission;
	
	@Override
	public PermissionDto getPermission(String kdObjectModulAplikasi, Integer kdKelompokUser, String kdModulAplikasi, Integer kdProfile) {
		PermissionDto permissionDto = new PermissionDto();
		List<MapObjekModulToKelompokUser> toKelompokUser = permission.getPermission(kdObjectModulAplikasi, kdKelompokUser, kdModulAplikasi, kdProfile);
		if (CommonUtil.isNullOrEmpty(toKelompokUser) || CommonUtil.isNullOrEmpty(toKelompokUser.get(0))) {
			return null;
		}
		if (toKelompokUser.size() > 1) {
			StringUtil.outPrintln("PermissionService : warning, permission ini lebih dari satu -> "
					+ "Profile : " + toKelompokUser.get(0).getKdProfile()
					+ ", ModulAplikasi : " + toKelompokUser.get(0).getKdModulAplikasi() 
					+ ", ObjekModulAplikasi : " + toKelompokUser.get(0).getKdObjekModulAplikasi()
					+ ", KelompokUser : " + toKelompokUser.get(0).getKdKelompokUser());
		}
		permissionDto.setCetak(toKelompokUser.get(0).getCetak() == 1);
		permissionDto.setSimpan(toKelompokUser.get(0).getSimpan() == 1);
		permissionDto.setUbah(toKelompokUser.get(0).getUbah() == 1);
		permissionDto.setHapus(toKelompokUser.get(0).getHapus() == 1);
		permissionDto.setTampil(toKelompokUser.get(0).getTampil() == 1);
		return permissionDto;
	}

}
