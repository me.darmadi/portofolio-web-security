package com.portofolio;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.portofolio.constant.SecurityConstant;
import com.portofolio.dao.mapper.NotificationMessagingMapper;
import com.portofolio.domain.KelompokUser;
import com.portofolio.domain.NotifMessaging;
import com.portofolio.domain.NotifMessagingScheduler;
import com.portofolio.domain.ObjekModulAplikasi;
import com.portofolio.domain.Pegawai;
import com.portofolio.dto.PermissionDto;
import com.portofolio.dto.SessionDto;
import com.portofolio.dto.UserDto;
import com.portofolio.notification.MessagePublisher;
import com.portofolio.security.AppPermission;
import com.portofolio.security.TokenAuthenticationService;
import com.portofolio.service.LoginUserService;
import com.portofolio.service.PermissionService;
import com.portofolio.service.RuleObjekModulAplikasiService;
import com.portofolio.util.CommonUtil;
import com.portofolio.util.DateUtil;
import com.portofolio.util.StringUtil;


@Component
public class AppInterceptor implements HandlerInterceptor {

	//private final Logger LOG = LoggerFactory.getLogger(AppInterceptor.class);
	@Autowired
	TokenAuthenticationService tokenAuthenticationService;

	@Autowired
	LoginUserService loginUserService;

//	@Autowired
//	LoginUserService loginPasienService;

	@Autowired
	PermissionService permissionService;
	
	@Autowired
	RuleObjekModulAplikasiService objekModulAplikasiService;
	
	boolean byPass = true;
	boolean byPassRule = false;
	
	SessionDto sessionDTO;
	UserDto userDto;

//	private boolean contains(String source, String[] matchers) {
//
//		if (CommonUtil.isNullOrEmpty(source) || ArrayUtils.isEmpty(matchers)) {
//			return false;
//		}
//
//		for (int i = 0; i < matchers.length; i++) {
//			if (source.contains(matchers[i])) {
//				return true;
//			}
//		}
//
//		return false;
//	}

	private String filterUrlForm(String urlForm) {
		
		return urlForm;

//		String[] hasil = urlForm.split("/");
//		StringUtil.outPrintln(hasil.length);
//		StringBuilder potongan = new StringBuilder();
//		for (int i = 0; i < hasil.length && i < 3; i++) {
//			potongan.append(hasil[i]).append("/");
//		}
//		return potongan.toString();
	}

	private boolean checkAuthSecure(HttpServletRequest request, HttpServletResponse response, String alamatURLFormObjek,
			boolean supervisi) throws Exception {

		Authentication authentication = tokenAuthenticationService.getAuthentication(request);

		sessionDTO = loginUserService.toSessionDto(authentication.getName());
		userDto = loginUserService.getUser();

		if (CommonUtil.isNullOrEmpty(userDto)) {
			StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 108 : UserDto Kosong tidak diizinkan lanjut");
			response.setHeader("RequireSupervisor", "false");
			response.setHeader(SecurityConstant.MessageInfo.ERROR_MESSAGE, "UserDto Kosong");
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return false;
		}
		return true;
	}

	private int checkKelompokUserPermision(UserDto user, ObjekModulAplikasi objekModulAplikasi, int action) {
		KelompokUser kelompokUser;
		
		if (user.getSuKdUser() == 0L) {
			kelompokUser = user.getKelompokUser();
		}else {
			kelompokUser = user.getSuperKelompokUser();
		}
		
		int result = -2;

		if (CommonUtil.isNotNullOrEmpty(kelompokUser)) {
			
			result = -1;
			if (kelompokUser.getKdKelompokUser() == 1) {
				result = 1;				
				StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 133 : KelompokUser Supervisor, semua aksi diizinkan.");
			}else {
				PermissionDto modul = permissionService.getPermission(objekModulAplikasi.getKode(), kelompokUser.getKode(), userDto.getKdModulAplikasi(), userDto.getKdProfile());
				if (CommonUtil.isNotNullOrEmpty(modul)) {
					StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 137 : ObjekModulAplikasi cocok, periksa actionnya.");
					result = 0;
					switch (action) {
						case AppPermission.VIEW:
							if (modul.isTampil()) {
								result = 1;
							}
							break;
						case AppPermission.SAVE:
							if (modul.isSimpan()) {
								result = 1;
							}
							break;
						case AppPermission.UPDATE:
							if (modul.isUbah()) {
								result = 1;
							}
							break;
						case AppPermission.DELETE:
							if (modul.isHapus()) {
								result = 1;
							}
							break;
						case AppPermission.PRINT:
							if (modul.isCetak()) {
								result = 1;
							}
							break;
					}
				}
			}
		}

		return result;
	}
	
	private boolean checkPermission(HttpServletRequest request, HttpServletResponse response, String alamatURLFormObjek, int action) throws Exception {
		ObjekModulAplikasi objekModulAplikasi = null;
		
		try {
			objekModulAplikasi = objekModulAplikasiService.findByAlamatUrlForm(filterUrlForm(alamatURLFormObjek));
		}catch(Exception e) {			
			StringUtil.outPrintln("[ERROR] AppInterceptor.java baris 179 : URL "+alamatURLFormObjek+" duplikasi  ");
		}

		if (CommonUtil.isNullOrEmpty(objekModulAplikasi)){ 
			StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 183 : Sementara defaultnya diloloskan semuanya jika tidak ada di dalam daftar objek modul aplikasi " + alamatURLFormObjek);
			return byPass;
		}
		
		if (!checkAuthSecure(request, response, alamatURLFormObjek, false)) {
			return false;
		}
		
		int resultKelompokUser = checkKelompokUserPermision(userDto, objekModulAplikasi, action);
		
		if (resultKelompokUser == -2){
			if (byPassRule) {
				return true; 
			}
			
			StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 198 : User "+sessionDTO.getNamaUser()+" tidak punya hak akses. User tidak berasosiasi dengan table KelompokUser. Tambahkan Kelompok Usernya.");
			response.setHeader("RequireSupervisor", "false");
			response.setHeader(SecurityConstant.MessageInfo.ERROR_MESSAGE, "User " + sessionDTO.getNamaUser() + " tidak punya hak akses.");
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return false;
			
		} else 	if (resultKelompokUser == -1){
			if (byPassRule) {
				return true;
			}
			
			StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 209 : User "+sessionDTO.getNamaUser()+" tidak punya hak akses. User tidak berasosiasi dengan table MapObjekModulToKelompokUser. Tambahkan hak akses kdObjekModulAplikasi di "+objekModulAplikasi.getKdObjekModulAplikasi()+" di MapObjekModulToKelompokUser.");
			response.setHeader("RequireSupervisor", "false");
			response.setHeader(SecurityConstant.MessageInfo.ERROR_MESSAGE, "User " + sessionDTO.getNamaUser() + " tidak punya hak akses.");
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return false;
			
		} else if  (resultKelompokUser == 0) {
			if (byPassRule) {
				return true;
			}
			StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 219 : User "+sessionDTO.getNamaUser()+" membutuhkan supervisor untuk akses "+action+" di ObjekModulAplikasi "+objekModulAplikasi.getObjekModulAplikasi()+" dengan kdObjekModulAplikasi "+ objekModulAplikasi.getKdObjekModulAplikasi()+" ");
			response.setHeader("RequireSupervisor", "true");
			response.setHeader(SecurityConstant.MessageInfo.ERROR_MESSAGE, "Action membutuhkan supervisi.");
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return false;
			
		} else {
			StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 226 : (SuperVisi berhasil), Informasi disimpan dengan info User "+sessionDTO.getNamaUser()+" untuk akses "+action+" di ObjekModulAplikasi "+objekModulAplikasi.getObjekModulAplikasi()+" dengan kdModulAplikasi "+objekModulAplikasi.getKdObjekModulAplikasi()+" ");			
			response.setHeader(SecurityConstant.MessageInfo.INFO_MESSAGE, "Supervisi berhasil.");
			response.setStatus(HttpServletResponse.SC_OK);
			return true;
		}	 
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		HandlerMethod hm;
		Method method = null;
		
		
		String URI = request.getRequestURI();

		
		boolean tdkBebasVisa = !URI.contains("HistoriAbsensi/") 
				&& !URI.contains("auth/") 
				&& !URI.contains(":9797") 
//				&& !URI.contains("auth/sign-in-pasien") 
//				&& !URI.contains("auth/sign-out")
//				&& !URI.contains("auth/sign-out-pasien")
//				&& !URI.contains("/dataMaster")
//				&& !URI.contains(":9292")
				&& !URI.contains(":9191")
				&& !URI.contains("swagger-ui.html")
				&& !URI.contains("/api/");
		
		if (!tdkBebasVisa) {
			//StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 255 : URL "+URI+" ini bebas visa");			
			return true;
		}

		
		try {
			String infoHeader = response.getHeader("secure");
			String info = loginUserService.getSession().getInfo();
			
			if (infoHeader != null && info != null && info.equals(infoHeader) && infoHeader.length() == 10) {
				StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 265 : Login Mobile ini bebas visa ");
				return true;
			}
		}catch(Exception e) {			
		}
		response.setHeader("Access-Control-Expose-Headers", "RequireSupervisor" + "," + SecurityConstant.MessageInfo.ERROR_MESSAGE + "," + SecurityConstant.MessageInfo.INFO_MESSAGE);
		String alamatURLFormObjek = request.getHeader(SecurityConstant.HttpHeader.URL_FORM) == null ? "": request.getHeader(SecurityConstant.HttpHeader.URL_FORM);
		String KdRuangan = "0";
		
		try {
			KdRuangan =  loginUserService.getSession().getKdRuangan(); //request.getHeader(SecurityConstant.HttpHeader.KD_RUANGAN) == null ? "0": request.getHeader(SecurityConstant.HttpHeader.KD_RUANGAN);
		}catch(Exception e) {			
		}
		
		int action = AppPermission.VIEW;
				
		if (CommonUtil.isNullOrEmpty(alamatURLFormObjek)){
			return byPass; // Lewatin dulu sementara;
		}
		
		if (CommonUtil.isNullOrEmpty(KdRuangan) || "0".equals(KdRuangan)){
			StringUtil.outPrintln("[INFORMASI SECURITY] AppInterceptor.java baris 286 : Sementara defaultnya diloloskan semuanya di AppInterceptor jika tidak ada header 'KdRuangan' atau header 'KdRuangan'nya 0 ..");
			return byPass; // Lewatin dulu sementara;
		}

		try {
			if (handler instanceof org.springframework.web.method.HandlerMethod) {
				hm = (HandlerMethod) handler;
				method = hm.getMethod();
				userDto = loginUserService.getUser();
			} else {
				return checkAuthSecure(request, response, alamatURLFormObjek, true);
			}

			boolean usingAnno = method != null && method.isAnnotationPresent(AppPermission.class);
						
			if (usingAnno) {
				if (method.getAnnotation(AppPermission.class) != null) {
					action = method.getAnnotation(AppPermission.class).value()[0];
				}

				if (AppPermission.SPECIALS == action) {
					return true;
				}
			}
			
			if (action == AppPermission.VIEW) {
				return byPass;
			}

			if (usingAnno && tdkBebasVisa) {				
				if (method.getAnnotation(AppPermission.class) != null) {
					action = method.getAnnotation(AppPermission.class).value()[0];
				}
				return checkPermission(request, response, alamatURLFormObjek, action);
			}
			

		} catch (Exception e) {
			StringUtil.printStackTrace(e);
			StringUtil.outPrintln("AppInterceptor baris 324 : URL tidak ditemukan " + request.getRequestURL());
			StringUtil.outPrintln("AppInterceptor baris 325 : " + e.getMessage());
			response.setHeader(SecurityConstant.MessageInfo.ERROR_MESSAGE, "Ada kesalahan dalam autorisasi.");
			response.setHeader("RequireSupervisor", "false");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return false;
		}

		// TUTUP DULU PENGGUNAANNYA...
		return byPass;
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
	}
	
	@Autowired
	NotificationMessagingMapper messagingMapper;
	
	@Autowired
	MessagePublisher<String, Object> messagePublisher;
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		
		HandlerMethod hm;
		Method method = null;
		
		if (!(handler instanceof org.springframework.web.method.HandlerMethod)) {
			return;
		}
		
		hm = (HandlerMethod) handler;
		method = hm.getMethod();
		
		boolean usingAnno = method != null && method.isAnnotationPresent(AppPermission.class);
		
		int action = AppPermission.VIEW;
		
		if (usingAnno) {
			if (method.getAnnotation(AppPermission.class) != null) {
				action = method.getAnnotation(AppPermission.class).value()[0];
			}
		}
		
		if (action == AppPermission.VIEW || AppPermission.SPECIALS == action) {
			return;
		}
		
		String URI = request.getRequestURI();
		
		boolean signInOut = !URI.contains("auth/sign-in") 
				&& !URI.contains("auth/sign-in-pasien") 
				&& !URI.contains("auth/sign-out")
				&& !URI.contains("auth/sign-out-pasien")
				&& !URI.contains("swagger");
		
		if (URI.contains("/api/") || !signInOut) {
			return;
		}
				
		String AlamatUrlForm = request.getHeader(SecurityConstant.HttpHeader.URL_FORM) == null ? ""
				: request.getHeader(SecurityConstant.HttpHeader.URL_FORM);
						
//		String KdRuangan = request.getHeader(SecurityConstant.HttpHeader.KD_RUANGAN) == null ? ""
//				: request.getHeader(SecurityConstant.HttpHeader.KD_RUANGAN);
		
		String cariJabatan = request.getHeader(SecurityConstant.HttpHeader.CR_JABATAN) == null ? ""
				: request.getHeader(SecurityConstant.HttpHeader.CR_JABATAN);
		
		String idRuanganTujuan = request.getHeader(SecurityConstant.HttpHeader.KD_RUANGAN_T) == null ? ""
				: request.getHeader(SecurityConstant.HttpHeader.KD_RUANGAN_T);
		
		String idRuanganTujuanA = request.getHeader(SecurityConstant.HttpHeader.KD_RUANGAN_A) == null ? ""
				: request.getHeader(SecurityConstant.HttpHeader.KD_RUANGAN_A);
		
		String tglKirim = request.getHeader(SecurityConstant.HttpHeader.TGL_KIRIM) == null ? ""
				: request.getHeader(SecurityConstant.HttpHeader.TGL_KIRIM);
		
		boolean notHttpOK = response.getStatus() >= HttpServletResponse.SC_BAD_REQUEST;
//		boolean notLewatMenu = CommonUtil.isNullOrEmpty(AlamatUrlForm) || CommonUtil.isNullOrEmpty(KdRuangan);
		
		
		boolean adaError = CommonUtil.isNotNullOrEmpty(ex);
		if (notHttpOK || adaError){
			return;
		}

		Authentication authentication = tokenAuthenticationService.getAuthentication(request);
		
		if (CommonUtil.isNullOrEmpty(authentication)){
			return;
		}
		
		SessionDto sesDto =  loginUserService.getSession();
		Pegawai pegawai = loginUserService.getPegawai(sesDto);
		
		if (CommonUtil.isNullOrEmpty(sesDto)) {
			return;
		}	
						
		String potongan = filterUrlForm(AlamatUrlForm);
		StringUtil.outPrintln("URL FORM  " + potongan);
		ObjekModulAplikasi objekModulAplikasi = objekModulAplikasiService.findByAlamatUrlForm(potongan);

		if (CommonUtil.isNullOrEmpty(objekModulAplikasi)){ 
			return;
		}
			
		String kdObjekModulAplikasi = objekModulAplikasi.getKdObjekModulAplikasi();
		
		List<NotifMessaging> messagings = messagingMapper.findNotifMessagingByObjekModulAplikasi(sesDto.getKdProfile(), sesDto.getKdModulAplikasi(), kdObjekModulAplikasi);
		
		if (CommonUtil.isNullOrEmpty(messagings)) {
			return;
		}
		
		List<Integer> kdMessagings = messagings.stream().map(m -> m.getKdNotifMessaging()).collect(Collectors.toList());
		
		String kdPegawai = sesDto.getKdPegawai();
		String kdRuangan =  sesDto.getKdRuangan();
		Integer kdProfile = sesDto.getKdProfile();
		
		if (CommonUtil.isNullOrEmpty(tglKirim)) {
			if (kdMessagings.size() > 0) {
				List<String> ruanganTujuansId = CommonUtil.createList();
				StringUtil.outPrintln("--");
				
				if (!"".equals(idRuanganTujuan)){
					JSONArray ruanganTujuanIds = new JSONArray(idRuanganTujuan);
					for (int i=0; i<ruanganTujuanIds.length(); i++){
						String ruanganTujuanId = ruanganTujuanIds.optString(i, "0");
						ruanganTujuansId.add(ruanganTujuanId);
					}	
				}
				
	//			if (!"".equals(idRuanganTujuanA) && ruanganTujuansId.isEmpty()) {
	//				JSONArray ruanganTujuanIds = new JSONArray(idRuanganTujuanA);
	//				for (int i=0; i<ruanganTujuanIds.length(); i++){
	//					String ruanganTujuanId = ruanganTujuanIds.optString(i, "0");
	//					ruanganTujuansId.add(ruanganTujuanId);
	//				}
	//			}
				
				MessagePublisher.RabbitHole rabbitHole = messagePublisher.getRabbitHole();
							
				try {
					rabbitHole.sendNotif(rabbitHole, sesDto, kdProfile, kdRuangan, pegawai, messagingMapper, kdMessagings,
							ruanganTujuansId, CommonUtil.isNotNullOrEmpty(cariJabatan));
				} catch (Exception e) {
//					StringUtil.printStackTrace(e);
					StringUtil.outPrintln("AppInterceptor baris 479 " + DateUtil.now() + " send notif dari kdRuangan "+sesDto.getKdRuangan()+" ada masalah " +e.getMessage());
				}
			}
		} else {
			try {
				JSONArray tglKirims = new JSONArray(tglKirim);
				JSONArray ruanganTujuanIds = new JSONArray(idRuanganTujuan);
				SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
				Integer kdMessaging = kdMessagings.get(0);
				for (int i=0; i<tglKirims.length(); i++){
					String ctglKirim = tglKirims.optString(i, "");
					//String cRuanganTujuanId = ruanganTujuanIds.optString(i,"0");
					Date date = myFormat.parse(ctglKirim);
					
					
					NotifMessagingScheduler vo = new NotifMessagingScheduler();
					
					vo.setTglKirim(DateUtil.convertDate(date));
					vo.setTerkirim(false);
					vo.setKdMessaging(kdMessaging);
					vo.setKdRuangan(kdRuangan);
					//vo.setKdRuanganTujuan(cRuanganTujuanId);
					vo.setKdPegawai(kdPegawai);					
					vo.setKdProfile(kdProfile);
					vo.setStatusEnabled(1);
	
					messagingMapper.addNotifMessagingScheduler(vo);
				}
			}catch(Exception e) {
				StringUtil.outPrintln("AppInterceptor baris 509 " + DateUtil.now() + " ex : " + e.getMessage());
//				StringUtil.printStackTrace(e);
			}
		}
	}
}	
