package com.portofolio.notification;

import java.util.List;
import java.util.Map;

import com.portofolio.dao.mapper.NotificationMessagingMapper;
import com.portofolio.domain.Pegawai;
import com.portofolio.dto.SessionDto;


public interface MessagePublisher<T, V> {	
	public String GetSettingDataFixed(String prefix);
	public void sendDirectNotification(final Map<T,V> data);
	
	public RabbitHole getRabbitHole();
	
	public static interface RabbitHole {
		public void connect(String host, String userName, String password, String routingKeyAndQueueName) throws Exception;
		public void sendRabbitMQNotification(String pesan) throws Exception;
		public void close() throws Exception;
		public void sendNotif(MessagePublisher.RabbitHole rabbitHole, SessionDto sess, Integer kdProfile, String kdDariRuangan, Pegawai pegawai,
				NotificationMessagingMapper notificationMessagingMapper, 
				List<Integer> kdNotifMessaging, List<String> lKdRuanganTujuan, boolean jabatan) throws Exception;
	}
	
}
