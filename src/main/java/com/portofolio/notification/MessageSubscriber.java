package com.portofolio.notification;

import com.rabbitmq.client.Consumer;

public interface MessageSubscriber {
	public void startRabbitMQNotification(String host, String routingKeyAndQueueName) throws Exception;
	public void listenRabbitMQNotification(String routingKeyAndQueueName, Consumer consumer, boolean autoAck) throws Exception;
	public void stopRabbitMQNotification() throws Exception;
	public Consumer getDefaultConsumer();
}
